                           [ D E V E L O P I N G ]

			 http:/wwww.ptlink.net/Coders/
31 May 2000
(.2)
    fixed endless loop passing v-lines on server connections
    added gline reason to G-lined quit message (suggested by Talesin@TTnet)
    added IDENT lookup option on include/config.h
    (reported by john.lange at darkcore.net)
    added dialog configuration generator "netconfig" (by jig at PTlink.net)
    added "by" info to addvline/remvline
    now Q/G/V lines are not sent to U:Lined servers on connect 
    IRC Operators now bypass MAXCHANNELSPERUSER 
    removed ERR_USERONCHANNEL error on m_invite being sent to U:lined
    added MODE -r being sent to user on nick change
    changed SAMODE propagation 

27 May 2000
(.1)
configure
    hidded "make install" kill ouput (to avoid "no such pid" message)
    removed xargs use, (maybe Solaris ...no such command... ? )
include/config.h
    DEBUGMODE is now turned off by default
src/channel.c
    fixed CANALWAYS join upercasse If error
    fixed SVSJOIN propagation to (old) no token servers 
src/help.c
    added missing include <sys.h> for non DEBUGMODE compile
src/s_err.c
    updated available modes on server INFO
 			 
23 May 2000
[---------------------------] PTlink ircd 5.1.0 [---------------------------]
* Config renamed to configure
* fixed bug on HTC
* rearranged some code to remove warnings with -Wall
* services root changed to TADMIN (Technical Admin)
* removed dummy cptr from IsULine
* added network settings using netcfg.h
* parsing now uses CRC hashing -Shadow
* even without KICKPROTECTION Opers cannot be kicked with services down
* removed some useless check_registered 
* added Zombie user mode +z (cannot send/topic/kick)
* added connect number counter
* make process alphabetized
* Changed star protocol to ptlink TST3 protocol 
* added message i/o log on debug mode -x1
* removed jynx lines
* removed ifdef USE_SERVICES/NPATH/SENDQ_ALWAYS/VMS code
* all warnings fixed on DEBUGMODE compile
* Added socks check exemption lines (e:) from UltimateIRCd
* renamed ircd.conf to ircd.conf.orig and added short ircd.conf
    SNICK will now pass masked hostname for newhosted users
    SJOIN chan user modes parsing completely recoded
channel.c
    added m_svsjoin 
    only allow JOIN 0 from remote server
    removed channel lookup on /mode without #/& char
    commented all wasteful channel zombies code
    added admin chanmode (+a)
    added Oper/Admin only chanmodes +O/+A
    implemented KICKPROTECTION
    chan ops can always send to +c channels
    sa_mode can be used by Opers now
    added OPERKICK, IRC Operator can kick when services are down
    added Helper/TAdmin/Zombie replies on m_whois
    recoded m_sjoin parsing (what a mess)
    sending CTCP's to channels is now IRC oper restricted 
	(extra space fix suggested by codemstr)
    do not allow @+# messages from non op/voice/admin users
gzline.c
    added G-lines (code optimization and (ugly) goto replacement)
    added V-lines (to block DCC SEND on certain files)
help.c
    added /helpsys for topic help from text file data
    (check files: user.help and oper.help)
masking.c
    removed extra parameter from function hideme
    added help_host for helpers
res.c
    reduced lookup timeout/retries to make connects faster
s_confedit.c (from PTlink ircd 4)
    ADDCNLINE/DELCNLINE/ADDHLINE/DELHLINE/ADDULINE/DELULINE commands
s_err.c
    changed some text
s_users.c
    added +w (wallops) for compatibility sake
    added +B (user is a BOT/screen mode)
    m_svsmode from PTlink ircd
    removed all USE_SERVICES stuff    
    removed +I identified nick mode
    services admin (+a) can only be set via SVSMODE
    optimized m_mode 
    removed ident check
    implemented NOSELFKILL,NOOPERKILL
    added +v mode to block DCCs for virus infected users
    added NOCOLORQUITS filter
    added m_ircops from UltimateIRCd
    totally recoded mode_snick (SNICK code moved to s_users.c)
s_serv.c
    added m_news
    restricted m_map tu IRC opers
    added HIDE_ULINE option
    added s_desc to change server description from UltimateIRCd
send.c
    added sendto_news
configure
    added -Wall to compile settings
    changed default installation directory to $HOME/ircd
common.h
    added <string.h> to avoid missing prototypes warnings
    
[------------------] Based on Star5.10.fx4.Velocity IRCd [------------------]