                        	[ S T A B L E ]

			    http:/wwww.ptlink.net/Coders/

14 Jun 2000
(.3)
    removed debug notices
    updated help system
    updated available chanmodes

08 Jun 2000
(.2)
    optimized SJOIN parsing to avoid dummy channel mode propagation
    fixed sjoin bug (not sending merged key/limit channel modes)
    fixed whois reply for Services Administrator
    /whois using * is restricted to Opers (Anti Spammer)
    Services can now set +O/+A channel modes (like always should)
    Admins can see channel modes on /list now
    quit prefix replaced by "Quit: reason"
    added Zurna and bosna-chat to networks list

06 Jun 2000    
(.1)
    fixed SNICK propagation bug sending invalid hostnames            
    fixed bug causing gline reason corruption
	(reported by skunk@PTlink.net)
    fixed Makefile on /tools
	
[---------------------------] PTlink ircd 5.2.0 [---------------------------]
* add TTnet to networks list
* added /tools (mkpasswd,fixklines) from Bahamut  
* fixed bug on hostname handling (i hope)
channels.c
    fixed samode bug (crashing with no parameters)
    added +S (NoSpam) mode to block *www.* *http* and */server* messages
    (suggested by winsome@zurna.net)
[---------------------------] PTlink ircd 5.1.2 [---------------------------]
