                        	[ S T A B L E ]

			    http:/wwww.ptlink.net/Coders/
22 Jul 2000			    
(.1)
    fixed ./configure script problem on some sh versions
    (Thanks to Yasin KIRICI for giving me access to his shell)
    domainname for /stats w is now retrieved from M:line - Shadow
    fixed dummy cannot join message beeing sent on operbypass 
    SNICK is now used to introduce new users (like it always should)
    fixed display bug reporting -r mode change for unregistered nicks
    
02 Jul 2000	
[---------------------------] PTlink ircd 5.3.0 [---------------------------]
+ Added Admin /helpsys (file doc/help.admin)
+ Added check for ircd running cron script (ircdchk)
channels.c
    fixed OPERBYPASS message displaying invalid username
gzline.c
    fixed remgline not found message been sent to remote server
    (reported by CrNiLaBud@BosnaChat)
help.c
    fixed invalid message of "Topic not found" on some situations
s_conf.c
    made help system files being reread on server rehash
    * on O:line flags will now include +A
    (suggested by Talesin@TTnet)
s_confedit.c        
    added /DISPLAY,/WRITE,MKPASSWD command from Elite IRCd
    (suggested by Talesin@TTnet)
s_user.c
    fixed (not working) NOCOLORQUITS code
    masked hostname display on kill message sent to killed victim
    (reported by Talesin@TTnet)
s_serv.c
    added /LNOTICE :message (suggested by FactorX@PTlink.net)    
    added umode +k message sending on SVSKILL
[---------------------------] PTlink ircd 5.2.3 [---------------------------]
