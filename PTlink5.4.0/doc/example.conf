# IRC - Internet Relay Chat, doc/example.conf
# Copyright (C) 1994, Helen Rose
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 1, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# This is an example configuration file for the irc2.8.21mu3.dal3
# and higher IRC servers.
# 
# You only need an ircd.conf (IRC server configuration file) if you are
# running an IRC server. If you are running a standalone client this file
# is not necessary.
#
# This file will explain the various lines in the IRC server
# configuration file. Not all lines are mandatory. You can check to make
# sure that your configuration file is correct by using the program
# "chkconf", provided in the server distribution (and when you do "make
# install" this program will be installed in the same directory as the irc
# server). 
#
# This document modified for use by the DALnet IRC network and its upgraded
# server versions by Russell on 10/22/95.
#
# This document updated to comform to current ircd changes and features by
# Cabal95 on 06/21/97.

# ========================================================================
# NOTE! this entire configuration file is read UPSIDE-DOWN! So if you have
# to put something in a specific order (for example, client-connection
# lines), put them in reverse order!
# ========================================================================

# Short ircd.conf by Lamego on 24/04/2000

#
# M: This line sets your server's name, description, port
# number, and IP address to bind to. 
#
# M:servername:IP:Description Of Your Server:port
#
M:server.some.net:*:Some net Server:6667
#
# A: This line lists your administrative information
# (contact address, etc). To view this information, /admin (server) will
# show it to you.
#
A:Generic Internet Access:Admin John Doe:jdoe@generic.com
#
# Y: These lines define connection classes. Connection
# classes allow you to fine-tune your client and server connections. It is
# suggested that clients and servers be placed in seperate classes, and if
# you have lots of server connections (if you do have lots of servers you
# shouldn't be reading this file :-) each set of servers (defined
# arbitrarily by you) should have its own class. If you have clients
# coming in from lots of different sites, you may want to seperate them
# out into classes. For instance, you may want to put local users in one
# class, with remote users in another class.
#
# The class numbers are not arbitrary. In auto-connecting servers -- that
# is, servers that you have a port number (e.g. 6667) on the end of the C:
# line (see below) the higher the number the higher the priority in
# auto-connecting.
#
# The fields in order are: class number, ping frequency (in seconds),
# connect frequency (in seconds), maximum number of links (used for
# auto-connecting, and for limiting the number of clients in that class),
# and sendq (this overrides any value set in include/config.h for #define
# MAXSENDQLENGTH).
#
# Note that it is a good idea to have ping frequency the same at both ends
# of the link.
# 
# in this case, connect-frequency is 0 indicating that this is a client
# class (servers never connect to clients, it is the other way around).
Y:1:90:0:250:100000
#
# Class 50 - Hub to hub, autoconnect
Y:50:90:60:1:4000000
#
# Class 51 - Hub to hub, no autoconnect
Y:51:90:60:0:4000000
#
# Class 30 - Hub to leaf
Y:30:180:0:0:3500000 
#
# Class 40 - leaf to hub, autoconnect
Y:40:90:90:1:3500000
#
# I: lines are client-authorization lines. Without
# these lines, no clients will be able to connect to your server.
# Wildcards ("*") are permitted. Passwords are also permitted (clients can
# be configured to send passwords).
# 
# Fields are as follows:
# I:IP-address-mask:optional password:domain-mask::connection class (opt)
#
# With a password..... This will allow anyone from anywhere to connect
# as long as they know the password ("foobar"). Note listing this I: line
# first, it will be read *last*, meaning it is the "fall-through". That
# is, anyone who doesn't match the I: lines listed below must know the
# password ("foobar") to connect.
#
# Password connections
#I:*@*:foobar:*@*::1
#
# Accept any client connection
I:*@*::*@*::1
#
# O: These lines define operator access. You do not need to
# have an operator to run a server. A well configured leaf site should not
# need an operator online, if it's connections are well defined, the irc
# administrator can use kill -HUP on the ircd to reload the configuration
# file. 
# The fields are as follows:
# O:hostname (ident "@" permitted):password:NickName:AccessFlags:class
# if the person in "NickName" is not coming from the hostname defined in
# the first field then the person will get the error message "No O: lines
# for your host". 
# NOTE that since Crypted Passwords are defined by default in
# include/config.h this text probably will not be plaintext. See
# ircd/crypt/README for more information.
#
# class is the Y:Line class you want this operator to end up in after they
# have successfully /oper'd.
#
# Access flags may be left blank, or * to give full access rights. Flags
# are in the form of single characters making a string.  Any combination
# of the following can be used(these are cAsE sensitive characters):
#
# r = access to /rehash server
# R = access to /restart server
# D = access to /die server
# h = oper can send /help ops
# g = oper can send /globops
# w = oper can send /wallops
# l = oper can send /locops
# c = access to do local /squits and /connects
# C = access to do remote /squits and /connects
# k = access to do local /kills
# K = access to do global /kills
# b = oper can /kline users from server
# B = oper can /unkline users from server
# n = oper can send local server notices(/notice $servername message)
# N = oper can send global notices(/notice $*.dal.net message)
# A = admin - currently does nothing
# u = oper can set /umode +c
# f = oper can set /umode +f
# o = local oper, flags included: rhgwlckbBnuf
# O = global oper, flags included: oRDCKN
#
# This is a plain vanilla O:line:
O:*.toledolink.com:nopassword:Russell:*:1
#
# This line is a generic "local operator",
# permits the nickname "jhs" with the password of "ITBites" to
# be a local operator only (be able to issue commands locally -- can /kill
# and /squit and /connect -- but *only* locally)
#
O:*.something.net:ITBites:jhs:o:10
#
# a crypted password line (NOTE that if you have crypted passwords, *all*
# of you passwords must be crypted!
#
O:rmiller@*.toledolink.com:T0eiVgHrqeKTQ:Russell::10
#
# U: This line defines the servers that IRC recognizes as being
# allowed to make various changes to the IRC environment (mode changes, for
# example), without complaining or otherwise obstructing them. For example,
# IRC services requires the following line:
U:services.some.net:*:*
#
# X: This line defines the password that an operator must use
# to restart or die the server. Note that they still myst have they R and D
# flags. This is just a security precaution against  accidentaly typing
# /die or /restart
# X:<password for /DIE>:<password for /RESTART>
X:diepass:restartpass

# C: These lines define what servers your server tries to
# connect to. 
# N: These lines define what servers your server permits
# connections to be initiated from. 
# C/N lines MUST be used in pairs. You cannot have one without the other. 
#
# C: lines contain the following fields:
# C:remote server's hostname:passwd:remote server's name:port:conn class
# (connection class)
# N: lines contain the following fields:
# N:remote server's hostname:passwd:remote server's name:host mask:conn class
# (connection class)
# "host mask" is the number of parts in *your* hostname to mask to. For
# instance, with my servername being "csa.bu.edu", if I wanted to present
# my servername to be "*.bu.edu" I would have a host-mask portion of "1". 
#
# it is *strongly* advised that your C/N line passwords be different for
# security's sake.
#
C:otherserver.some.net:somenetpass:other.servername.net:6667:30
N:otherserver.some.net:somenetpass:other.servername.net::30
#
#
# K: These lines define user@host patterns to be banned from
# this particular server (with an optional time field). Note that K: lines
# are *not* global, and if you ban a user they can still use any other IRC
# server (unless they have specifically been banned there as well).
#
# The time field (same as reason) is only used if you #define
# TIMED_KLINES in config.h, and even then they are discouraged.
# 
# the fields are defined as:
# K:hostmask:reason:username
# wildcards are permitted in any one of the fields, in other words, you can
# K:*::* if you wanted (but your server wouldn't be used much ;-)
#
# This K: line bans the username "hrose" (the wildcards are used to make
# sure that any ident-checking character will match) on any machine from
# the University of Boston.
K:*.bu.edu:Hacking #dragonrealm:*hrose*
#
# This K: line bans any users from acs*.bu.edu between the hours of 8am
# and 12pm and 1pm and 5pm (the time is always the server's local time).
# Time-based K-lines such as this require TIMED_KLINES in config.h to be
# #defined.
K:acs*.bu.edu:0800-1200,1300-1700:*
# Note that 24 hour time is used (no "AM" or "PM").
#
# Q: Different from the above type of q: line, these lines
# prevent users on your server from picking certain nicks. This is useful
# if your network has 'reserved' nicknames that should not be taken by
# normal users.  Note the difference in capitalization and BE CAREFUL!
#
# The fields are as follows:
# Q:*:reason why quarantine is in place:nickname
#
# For example, with PTlink Services should have:
Q::Reserved for services:ChanServ
Q::Reserved for services:NickServ
Q::Reserved for services:MemoServ
Q::Reserved for services:OperServ
Q::Reserved for services:HelpServ
Q::Reserved for services:NewsServ
Q::Reserved for services:Global
#
Q::Reserved for operators:IRCop
#
# 
# Z: These block ALL incoming connections from a certain
# IP address mask.  They can stop users who log on and issue the SERVER
# command a couple of times just to annoy irc ops.  They are more
# powerful that K-lines because they can stop users before they've had a
# chance to register.  This works on servers, SO BE VERY CAREFUL WITH
# YOUR Z-LINE MASKS.  Z-lines are a LAST RESORT.  They are much too
# dangerous and powerful to be using them instead of K-lines and akills.
#
# NOTE 1: Z-lines do NOT work on host names!  Only IP addresses.
# NOTE 2: Z-lines are part of /stats k.  
# NOTE 3: The :* at the end is REQUIRED.  Not using it will cause both
# ircd and chkconf to segfault without saying why.
#
# Syntax:
# Z:IP mask:reason for zapping:*
#Z:127.0.0.1:Please don't log on from the local machine:*
#
# L: These lines "Leaf" specified servers. They are only
# useful if you are a non-leaf site yourself. There are two ways you can
# use L: lines. The first will limit one particular site to a particular
# tree depth (including 0, which would mean the server has to connect with
# no servers linked behind it otherwise the connection will fail). The
# second will allow you to be selective about which other servers you wish
# the connecting server to behave as a leaf towards.
#
# The fields are as follows:
# L:disallow connections to this hostmask::server name:depth
# For example, this will force kaja.gi.alaska.edu to connect only as a
# leaf (if it is not a leaf, the link will be dropped):
#L:::kaja.gi.alaska.edu
# This line will force cm5.eng.umd.edu to have a depth of only 1 below it
# (that is, it is allowed to have only leaves connected to it):
#L:::cm5.eng.umd.edu:1
#
# This line will prohibit anything matching *.edu to be connected behind
# any server matching *.au:
#L:*.edu::*.au
#
# H: These lines define who you permit to act as a "hub" to
# you (that is, who you permit to connect non-leafed servers to you).
#
# the first field may use wildcards, the third field *must* be an exact
# match for a server's name (NOT a server's hostname, if they differ, the
# server's name must be used). If the servername is a wildcard (e.g. *.au)
# that is an acceptable name for the third field. 
#
# The fields are as follows:
# H:servers which are permitted entry::hub server
#
# Example, permit mindijari.ca.us.dal.net to allow any servers behind it to 
# connect: 
H:*::nowhere.he.us.dal.net
#
# P: This field allows the server to listen on various ports
# (other than 7000) for connections. Any internet domain port that is
# below 1024 means the ircd has to be run from inetd. The server can
# listen to ports in the UNIX domain or the internet domain. If you wish
# to create a port in the UNIX domain you must compile with UNIXPORT
# defined in include/config.h. If you are permitting connections to a
# seperate port, you can control access to that port by the host field.
#
# You can now bind internet ports to specific IP interface devices.  To do
# this put the IP address of the interface in the host field.  To bind to
# all interfaces, leave empty or put * in.
#
# The fields are as follows::
# P:IP# or UNIX socket file:*:*:port number
# for example, an internet domain socket on port 6665 for the local
# computer
#P:127.0.0.1:*:*:6665
#
# e: socks exemption lines
# The servers connection from this ip will not be checked for open socks
# e:IP::*
e:127.0.0.1::*
