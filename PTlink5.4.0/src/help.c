/****************************************************
 * help.c - /helpsys related routines               *
 ****************************************************
 * (C) PTlink Coders Team 2000 - Lamego@PTlink.net  *
 ****************************************************/

#include "config.h"
#include "struct.h"
#include "h.h"
#include "help.h"
#include "sys.h"
#include <stdio.h>
#include <string.h>
extern HelpItem	*user_help, *oper_help, *admin_help;	/* help systems */

/*
   ** m_helpsys	by Lamego (May 2000)
   **      parv[0] = sender prefix
   **	   parv[1] = topic
*/ 
int m_helpsys(aClient *cptr, aClient *sptr, int parc, char* parv[])
{
    char** ht; 
    int found=0;
    if((ht=help_find(parc>1 ? parv[1] : "",user_help))) {
	found=1;
	sendto_one(sptr, ":%s NOTICE %s :**************** \2USER HELP SYSTEM\2 ***************",
		    me.name, sptr->name);        
	while(*ht)
	    sendto_one(sptr, ":%s NOTICE %s :%s",
		   me.name, sptr->name, *(ht++));    
	sendto_one(sptr, ":%s NOTICE %s :*************************************************",
		   me.name, sptr->name);        		   
    }
    if(IsAnOper(sptr) && (ht=help_find(parc>1 ? parv[1] : "",oper_help))) {
	found=1;    
	sendto_one(sptr, ":%s NOTICE %s :**************** \2OPER HELP SYSTEM\2 ***************",
		   me.name, sptr->name);            
	while(*ht)
	    sendto_one(sptr, ":%s NOTICE %s :%s",
		   me.name, sptr->name, *(ht++));
	sendto_one(sptr, ":%s NOTICE %s :*************************************************",
		   me.name, sptr->name);        
		   
    } 
    if(IsAdmin(sptr) && (ht=help_find(parc>1 ? parv[1] : "",admin_help))) {
    	found=1;
	sendto_one(sptr, ":%s NOTICE %s :**************** \2ADMIN HELP SYSTEM\2 ***************",
		   me.name, sptr->name);            
	while(*ht)
	    sendto_one(sptr, ":%s NOTICE %s :%s",
		   me.name, sptr->name, *(ht++));
	sendto_one(sptr, ":%s NOTICE %s :*************************************************",
		   me.name, sptr->name);        
		   
    }        
    if(!found)
	sendto_one(sptr, ":%s NOTICE %s :Help topic not found",
	    sptr->name, me.name);    
    else if(!IsAnOper(sptr))
	sptr->since += 7; /* extensive help can lag server */
    return 0;	    
}

/******************************************
    Loads a help system from a text file
 ******************************************/
int help_load(char *fn, HelpItem **hi) {
    char *hl[128]; /* buffer to store a full item */
    char line[256]; /* one line buffer */
    char topic[32]; /* current topic */
    char *tmp;
    int	 total;    /* total lines for the current item */
    FILE *helpfile;
    HelpItem *h,*last;   /* current,last help item */    
    *hi = NULL;
    if(!(helpfile = fopen(fn,"rt"))) {
	return 1;
    }
    h = (HelpItem *) MyMalloc(sizeof(HelpItem));
    (*hi)=h;
    last=NULL;
    topic[0]='\0';
    total=0;
    while(fgets(line,sizeof(line),helpfile)) {
	if ((tmp = (char *) index(line, '\n')))
	    *tmp = '\0';
	if ((tmp = (char *) index(line, '\r')))
	    *tmp = '\0';	    
	if(line[0]=='*') {
	    if(total) {
		h->topic = strdup(topic);
		h->text = malloc(sizeof(char*) * (total+1));
		h->text[total]=NULL;	/* mark end of text*/
		while(total--) {
		    h->text[total] = strdup(hl[total]);
		};
		h->next = (HelpItem *) MyMalloc(sizeof(HelpItem));
		last=h;
		h=h->next;
		total=0;
	    }
	    strncpy(topic,&line[1],sizeof(topic)); /* save topic*/
	} else 
	    hl[total++]=strdup(line);
    }
    free(h); /* Last help is *EOF ... lets discard it */
    last->next=NULL; /* mark end of list*/
    fclose(helpfile);
    return 0;
}

/******************************************
    Looks for a topic on a help system
    returns help message or NULL (not found)
 ******************************************/
char** help_find(char* topic, HelpItem *hi) {
    HelpItem *h = hi;
    while(h) { 
	if(!strcasecmp(h->topic,topic)) 
	    return h->text;
	else    
	    h=h->next;
    }	    
    return NULL;
}

/***************************************************
    Fress all the memory alocated to an help item
 ***************************************************/
void help_free(HelpItem **hi) {
    char** ht; 
    HelpItem *h = *hi;
    HelpItem *tofree;
    while(h) {
	free(h->topic);
	ht=h->text;
	while(*ht) {
	    free(*(ht++));
	}
	free(h->text);
	tofree = h;
	h=h->next;
	MyFree(tofree);
    }
    (*hi)=NULL;
}