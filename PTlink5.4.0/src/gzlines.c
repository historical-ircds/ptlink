/************************************************************************
 *   IRC - Internet Relay Chat, src/gzlines.c
 *   Copyright (C) 1999 Maarten van den Bosch
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ************************************************************************/

#include "netcfg.h"
#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "msg.h"
#include "channel.h"
#include "userload.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <utmp.h>

#include "h.h"
/* G-lines stuff */
aGline   *expire_gline (aGline *tmp);
void    check_expire_gline (void);
aGline	*glines = NULL;
aVline	*vlines = NULL;
void add_vline(char *mask, char *reason);
aVline *del_vline(aVline *p);
/* GZ-lines */
static int advanced_check(char *, int);
aGZline *GZlineList = NULL;

#define AllocCpy(x,y) x = (char *) MyMalloc(strlen(y) + 2); strcpy(x,y)
#define GFreeStr(x) MyFree((char *) x)
#define GFreeGline(x) MyFree((aGline *) x)

/*
 * add_gline
 *   adds a gline to the linked list
 *  coded by Stskeeps
*/

void add_gline(usermask, hostmask, reason,setby, expire_at, set_at) 
char *usermask, *hostmask, *reason, *setby;
time_t expire_at, set_at;
{
	aGline *ng;
    /* acknowledged - no bugs */	
	ng = (aGline *) MyMalloc(sizeof(aGline));
	
	AllocCpy(ng->usermask, usermask);
	AllocCpy(ng->hostmask, hostmask);
	AllocCpy(ng->reason,   reason);
	AllocCpy(ng->setby,    setby);
	
	ng->expire_at 		 = expire_at;
	ng->set_at		= set_at;
	ng->prev		= NULL;
	ng->next		= glines;
	if (glines)
		glines->prev = ng;	
	glines = ng;
}


/* 
 * del_gline 
 *   coded & recoded by Stskeeps - recoded by Lamego
 *   return pointer to next if found
 *   NULL = no more glines
 *   
 */
aGline *del_gline(aGline* p)
{	
    aGline *q;
    if(!p) return NULL;
    q = p->next;
    GFreeStr(p->hostmask);
    GFreeStr(p->usermask);
    GFreeStr(p->reason);
    GFreeStr(p->setby);	
    if (p->prev) {
        p->prev->next = p->next;
    } else {
        glines = p->next;
    }
    if (p->next) {
	    p->next->prev = p->prev;
    }
    MyFree((aGline *) p);
    return q;			
}



/*
	find_gline
	by stskeeps (recoded) // recoded by Lamego
	returns NULL if cptr is not glined
*/

aGline *find_gline (cptr)
	aClient *cptr;
{
	char	*chost, *cname;
	aGline	*glp;
	time_t	nowtime;
		
	if (IsServer(cptr))
		return 0;
	
	check_expire_gline();

	nowtime = TStime();
	chost = cptr->sockhost;
	cname = cptr->user->username;	

	for (glp = glines; glp; glp = glp->next)
	{
		if ((!match(glp->hostmask, chost) && !match(glp->usermask, cname))
		|| 
		(!match(glp->hostmask, inetntoa((char *) &cptr->ip)) && !match(glp->usermask, cname)))
		{
			sendto_one(cptr, "NOTICE %s :*** You are banned for %li seconds. (reason: %s).",
					 cptr->name,
					 (glp->expire_at - TStime()),
					 glp->reason);
			return glp;
		}
	}
	return NULL;
}

void  check_expire_gline (void)
{
	aGline *gp, t;
	time_t nowtime;	
	nowtime = TStime();
	for (gp = glines; gp; gp = gp->next)
	{
		if (nowtime >= gp->expire_at ) 
		{
			t.next = expire_gline(gp);
			gp = &t;
		}
	}
}

aGline *expire_gline (aGline *tmp)
{	
	FILE *glinelog;
	
	if (!tmp)
		return NULL;
	if (tmp->expire_at > TStime()) {
		sendto_ops("expire_gline(): expire for not-yet-expired gline %s@%s",
			tmp->usermask,
			tmp->hostmask);
		return (tmp->next);	
	}

	sendto_umode(UMODE_OPER, "*** Expiring G-line (%s@%s) made by %s (Reason: %s)",
		tmp->usermask, tmp->hostmask, tmp->setby, tmp->reason);

	glinelog = fopen (GPATH, "a");
	if (glinelog == NULL) {
     		sendto_realops("Couldn't write to gline.log - Disk full?");
	} else {
    	    fprintf(glinelog, "(%s) - Expiring G-line %s@%s made by %s (Reason: %s)\n",
    		myctime(TStime()), tmp->usermask, tmp->hostmask, 
    		tmp->setby, tmp->reason); 
    	    fclose (glinelog);
	}
	return(del_gline(tmp));
}

void expire_glines ()
{
/*
 * Stskeeps: As we are using linked lists now this proc is unneeded
 *           will maybe be used later for a /GLINE EXPIREALL ?
 * 
	int	i;
	FILE *glinelog;

        sendto_realops("*** G-line maximal count reached, expiring"
			" all G:lines");
	for (i=0 ; i<MAXGLINE ; i++)
	{
		if (glinetab[i] != NULL)
		{
			MyFree (glinetab[i]);
			glinetab[i] = NULL;
		}
	}
	lastgline = 0;

        glinelog = fopen (GPATH, "a");
		if (glinelog == NULL) {
		 sendto_realops("Couldn't write to gline.log - Disk full?");
		 return;
		}
		fprintf (glinelog, "%s - FATAL: Maximum amount of G-lines reached -
			all G-lines expiring!\n", myctime(TStime()));
        fclose (glinelog);
*/

}
	
int	gline() {
	/* just sweeps net for people that should be killed */
	aClient *acptr;
		long i,i1;
	aGline *gl;
	check_expire_gline();

    for (i1 = 0; i1 <= 5; i1++) {
	for (i = 0; i <= (MAXCONNECTIONS -1) ; i++ ) 
	    if ((acptr = local[i]) && MyClient(acptr) && (gl=find_gline(acptr))) {
	        char *greason;
	        greason = MyMalloc(strlen(gl->reason)+45);
	        sprintf(greason,"User has been banned [G:Lined], reason: %s",gl->reason);
		exit_client (NULL, acptr, &me, greason);
		MyFree(greason);
	}
    }
    return 1;
}

/*
** dump_gline give list of current glines, times, and reasons
** Recoded by Stskeeps
*/
void dump_gline (aClient *sptr) {
	time_t curtime;
	aGline *p;

	curtime = TStime();
	check_expire_gline();
	for (p = glines; p; p = p->next) {
		sendto_one(sptr, rpl_str(RPL_STATSGLINE),me.name, sptr->name,'G',
			p->usermask, p->hostmask, (p->expire_at - curtime),
			(curtime - p->set_at),
			p->setby, p->reason);

	}
}

/*
** m_remgline		(removed all those bad coding gotos and added ALL -Lamego)
** parv[0] = sender
** parv[1] = hostmask
*/
int m_remgline (aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	char	*p;
	aGline 	*gl;
	FILE	*glinelog;

        if (!IsOper (sptr) && !IsULine(sptr)){
    	    sendto_one(sptr,err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
            return 1;
        }	
	if (parc < 2) {
    	    sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
        	me.name, parv[0], "REMGLINE");
            return 1;
        }
	if(!strcasecmp(parv[1],"ALL")) {
	    if (!IsServer(sptr))
		sendto_umode(UMODE_OPER,  "*** %s has removed all G:lines",sptr->name);		    
	    while(del_gline(glines));
	    sendto_serv_butone(cptr, ":%s REMGLINE ALL", sptr->name);	    
	    return 0;
	}
	p = strchr (parv[1], '@');
	if (!p) {
	    sendto_one (sptr,":%s NOTICE %s :*** Use a hostmask (G:Line)",
		me.name, parv[0]);
	    return 1;
	}
	*p = 0;
	check_expire_gline();
	gl = glines;
	while (gl) {
		if (!match(gl->hostmask, p+1) &&
		    !match(gl->usermask, parv[1]))
		{
			sendto_umode(UMODE_OPER,  "*** %s has removed G:Line for %s@%s",
					sptr->name,
					gl->usermask,
					gl->hostmask);
			sendto_serv_butone(cptr,
			    ":%s REMGLINE :%s@%s", sptr->name, gl->usermask, gl->hostmask);
       		        glinelog = fopen (GPATH, "a");
			if (glinelog == NULL) {
				sendto_realops("Couldn't open gline.log - Disk full?");
			} else {
			    fprintf (glinelog, "%s - %s removed G:line for %s@%s\n",
				myctime(time(NULL)), 
				sptr->name,
				gl->usermask,
				gl->hostmask);
      			    fclose (glinelog);
			}
	 		del_gline(gl);
			return 0;
		  }
	 	   else
		   	gl = gl->next;	
	}
	
	sendto_one (sptr, ":%s NOTICE %s :*** /RemGLine - No match for hostmask"
			" %s@%s",me.name,sptr->name,parv[1],p+1);
	return 0;
}

/*
** m_gline
** Changed expire broadcast to be supported by this our fake TStime ircd --Lamego 1999
** Recoded by Stskeeps
** Recoded SECOND time by Stskeeps
** parv[0] = sender
** parv[1] = nick or hostmask
** parv[2] = time

** parv[3] = reason (if !MyClient its Sender)
** parv[4] = reason (if !MyCLient)
*/

int m_gline (aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	/* client: GLINE mask secs :Reason */
	/* server: :server GLINE mask expire_at duration sender :Reason */
	char	*sender=NULL, *mask=NULL, *timestr=NULL, *reason=NULL, *set_at=NULL;
	char	*usermask, *hostmask, *say=NULL;
	char	*s, *p;
	char	gmt[256];
	char	gmtg[256];	
	char 	tempmask[256];
	aGline	*gl;
	time_t	curtime, secs, secs2, expire_at;
	FILE	*glinelog;
	long	i;
	if (parc == 1)
	{
		dump_gline(sptr);	
		return 0;
	}

	if (!IsOper(sptr) && !IsServer(cptr))
	{
		if (!IsServer(sptr))
			sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, sptr->name);
		return 0;
	}

	if (MyOper(sptr)) 
	{
		if (parc < 4)
		{
			sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
			    me.name, sptr->name, "GLINE");
			return 1;			
		}
		sender = s = make_nick_user_host(sptr->name, sptr->user->username, (IsHidden(sptr) ? sptr->user->mask : sptr->user->host));
		mask   = parv[1];
		timestr= parv[2];
		reason = parv[3]; 		
	}
	if (IsServer(sptr))
	{
		if (parc < 5)
		{
			sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
						me.name, sptr->name, "GLINE");
			return 1;
		}
		mask 	= parv[1];
		timestr = parv[2];
		set_at  = parv[3];

		sender  = parv[4];
		reason  = parv[5];
	}
        if (strlen(mask) > (HOSTLEN + USERLEN ) ) {
        sendto_one (sptr,":%s NOTICE %s :*** Gline mask too long",
                                me.name, parv[0]);
                return 0;
        }
        if (mask[0] == '@')
        {
                strcpy(tempmask, "*");
                strcat(tempmask, mask);
                mask = tempmask;
        }	 	
	/* Check if its a hostmask AND legal.. */
	p = strchr(mask, '@');
	if (!p)
	{
		sendto_one (sptr,":%s NOTICE %s :*** Use a hostmask (G:Line)",
				me.name, parv[0]);
		return 0;
	}
	if (p)
	{
		p++;
		i = 0;
		while (*p)
		{
			if (*p != '*' && *p != '.') i++;
			p++;
		}
			if (i<4)
			{
				sendto_umode(UMODE_OPER,"*** Addition of G:Line failed for %s (%s)",
					mask, sptr->name);
				return 0;
			}
	}
	/* user@host.. */
	usermask = strtok(mask, "@");
	hostmask = strtok(NULL, "@");

	check_expire_gline();

	gl = glines;
	
	while (gl != NULL) {
		if (!match(gl->hostmask, hostmask) &&
		    !match(gl->usermask, usermask))
		{
			if (!IsServer(sptr)) {
			    sendto_one(sptr,
			     ":%s NOTICE %s :*** Cannot add G:Line - Match already exists!", me.name, sptr->name);
			}
			return 0;
		}
		gl = gl->next;
	}

	/* No matches.. */
	curtime = TStime();
	secs = atol(timestr);		
	if (secs < 1) {
		sendto_one(sptr, ":%s NOTICE %s :*** G:Line - Please specify a POSITIVE value for expiry time", me.name, sptr->name);
		return 0;
	}
	
	secs2 = secs;
	if (secs < 60) {
		say = "second(s)";
	}
	if ((secs > 59) && (secs < 3600)) {
		secs2 = secs / 60;
		say = "minute(s)"; 
	}
	if ((secs > 3599) && (secs < 86400)) {
		secs2 = secs / 60 / 60;
		say = "hour(s)";
	}
	if ((secs > 86399) && (secs < 999999999)) {
		secs2 = secs / 60 / 60 / 24;
		say = "day(s)";	
	}
	if (secs > 999999998) {
		sendto_one (sptr, ":%s NOTICE %s :*** Couldn't add G:Line - Seconds value too large - Please decrease", me.name, sptr->name);
		return 0;
	}
	
	/* GMT clock. */
	strncpy (gmt, asctime(gmtime((clock_t*)&curtime)), sizeof(gmt));
	gmt[strlen(gmt)-1] = 0;

	expire_at = curtime + secs;
	strncpy (gmtg, asctime(gmtime((clock_t*)&expire_at)), sizeof(gmtg));
	gmtg[strlen(gmtg)-1] = 0;

	/* add_gline */
	add_gline(usermask, hostmask, reason, sender, expire_at, (IsServer(sptr) ? atol(set_at) : curtime));
	if (!IsServer(sptr)) 	
	sendto_umode(UMODE_OPER,
	    "*** G:line added for %s@%s on %s GMT (From %s for approximately %d %s : %s) will expire on %s",
	    usermask, hostmask, gmt, sender, secs2, say, reason,gmtg);

	sendto_serv_butone(cptr, ":%s GLINE %s@%s %li %li %s :%s",
		me.name,
		usermask, hostmask,
		secs,
		(IsServer(sptr) ? atol(set_at) : curtime),
		sender,
		reason);

	/* log it in the gline.log */
	glinelog = fopen (GPATH, "a");

	if (glinelog == NULL) {
		sendto_realops("*** Couldn't log to gline.log! Disk full?");
		goto nowrite;
	}

	fprintf(glinelog, "(%s) %s@%s G:line added by %s (%s) for %li %s\n",
		myctime(TStime()),
		usermask,
		hostmask,
		sender,
		reason,
		secs2,
		say);

	fclose(glinelog);	

	nowrite:		
	gline();
	
	return 1;

}    	
/*
    m_addvline	
    Adds a V-line string to block DCC SENDs for certain filename (masks are allowed)
    
    parv[0] = sender
    parv[1] = filename
    parv[2] = reason
*/ 
int m_addvline (aClient *cptr, aClient *sptr, int parc, char *parv[]) 
{    
    char mask[32];
    int i;
    if(!IsOper(sptr) && !IsServer(sptr)) {
	    sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, sptr->name);
	return 1;
    }
    if(parc==1) {
	report_vlines(cptr);
	return 1;
    }
    if(parc<3) {
	sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
	    me.name, sptr->name, "VLINE");
	return 1;
    }
    strncpy(mask,parv[1],31);
    i = strlen(mask);    
    if(mask[i-1]!='*')
        strcpy(&mask[i]," *"); /* DCC SEND filename ... */    
    if(find_vline(mask))
    {
	if (!IsServer(sptr)) {
    	    sendto_one(sptr, ":%s NOTICE %s :*** Cannot add V:Line - Match already exists!", me.name, sptr->name);
	}
	return 1;
    }
    add_vline(mask,parv[2]);
    if(!IsServer(sptr))
	sendto_umode(UMODE_OPER,"*** V:line added by %s for %s reason: %s",parv[0],parv[1],parv[2]);
    sendto_serv_butone(cptr,
	":%s ADDVLINE %s :%s",parv[0],parv[1],parv[2]);
    return 0;
}

/*
    m_remvline	
    Removes a V-line 

    parv[0] = sender
    parv[1] = filename
*/ 
int m_remvline (aClient *cptr, aClient *sptr, int parc, char *parv[]) 
{
    char mask[32];
    int i;
    aVline *vl;
    if(!IsOper(sptr) && !IsServer(sptr)) {
	sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, sptr->name);
	return 1;
    }
    if(parc<2) {
	sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
	    me.name, sptr->name, "VLINE");
	return 1;
    }
    strncpy(mask,parv[1],31);
    i = strlen(mask);    
    if(mask[i-1]!='*')
        strcpy(&mask[i]," *"); /* DCC SEND filename ... */    
    if(!(vl=find_vline(mask)))
    {
	if (!IsServer(sptr)) {
    	    sendto_one(sptr, ":%s NOTICE %s :*** V:Line not found", me.name, sptr->name);
	}
	return 1;
    }
    del_vline(vl);
    if(!IsServer(sptr))
	sendto_umode(UMODE_OPER,"*** V:line deleted by %s for %s",parv[0], parv[1]);    
    sendto_serv_butone(cptr,":%s DELVLINE %s :%s",parv[0],parv[1],parv[2]);	
    return 0;
}


void report_vlines(aClient *sptr)
{
    aVline *tmp = vlines;
    while(tmp) {
	sendto_one(sptr, rpl_str(RPL_STATSVLINE), me.name,
		       sptr->name, tmp->mask, tmp->reason);
	tmp=tmp->next;
    }
}

void add_vline(char *mask, char *reason) 
{
	aVline *nv;
    /* acknowledged - no bugs */	
	nv = (aVline *) MyMalloc(sizeof(aVline));
	if(strlen(reason)>128)
	    reason[127]='\0';
	AllocCpy(nv->mask, mask);
	AllocCpy(nv->reason, reason);
	nv->prev = NULL;
	nv->next = vlines;
	if (vlines)
		vlines->prev = nv;	
	vlines = nv;
}

aVline *del_vline(aVline *p)
{	
    aVline *q;
    q = p->next;
    GFreeStr(p->mask);
    GFreeStr(p->reason);
    /* chain1 to chain3 */
    if (p->prev) {
        p->prev->next = p->next;
    } else {
        vlines = p->next;
    }
    if (p->next) {
	    p->next->prev = p->prev;
    }
    MyFree((aVline *) p);
    return q;			
}


/*
	find_vline
	returns pointer to vline if filename is vlined
*/

aVline* find_vline (char* filename)
{
    aVline	*vlp;	
    for (vlp = vlines; vlp; vlp = vlp->next) {
	if (!match(vlp->mask, filename))
	    return vlp;
    }
    return NULL;
}



/* ok, given a mask, our job is to determine
 * wether or not it's a safe mask to banish...
 *
 * userhost= mask to verify
 * ipstat= TRUE  == it's an ip
 *         FALSE == it's a hostname
 *         UNSURE == we need to find out
 * return value
 *         TRUE  == mask is ok
 *         FALSE == mask is not ok
 *        UNSURE == [unused] something went wrong
 */

int advanced_check(char *userhost, int ipstat)
{
    register int retval = TRUE;
    char *up = NULL, *p, *thisseg;
    int numdots = 0, segno = 0, numseg, i = 0;
    char *ipseg[10 + 2];
    char safebuffer[512] = "";	/* buffer strtoken() can mess up to its heart's content...;> */

    strcpy(safebuffer, userhost);

#define userhost safebuffer
#define IP_WILDS_OK(x) ((x)<2? 0 : 1)

    if (ipstat == UNSURE) {
	ipstat = TRUE;
	for (; *up; up++) {
	    if (*up == '.')
		numdots++;
	    if (!isdigit(*up) && !ispunct(*up)) {
		ipstat = FALSE;
		continue;
	    }
	}
	if (numdots != 3)
	    ipstat = FALSE;
	if (numdots < 1 || numdots > 9)
	    return (0);
    }
    /* fill in the segment set */
    {
	int l = 0;
	for (segno = 0, i = 0, thisseg = strtoken(&p, userhost, "."); thisseg;
	     thisseg = strtoken(&p, NULL, "."), i++) {

	    l = strlen(thisseg) + 2;
	    ipseg[segno] = calloc(1, l);
	    strncpy(ipseg[segno++], thisseg, l);
	}
    }
    if (segno < 2 && ipstat == TRUE)
	retval = FALSE;
    numseg = segno;
    if (ipstat == TRUE)
	for (i = 0; i < numseg; i++) {
	    if (!IP_WILDS_OK(i) && (index(ipseg[i], '*') || index(ipseg[i], '?')))
		retval = FALSE;
	    MyFree(ipseg[i]);
    } else {
	int wildsok = 0;

	for (i = 0; i < numseg; i++) {
	    /* for hosts, let the mask extent all the way to
	       the second-level domain... */
	    wildsok = 1;
	    if (i == numseg || (i + 1) == numseg)
		wildsok = 0;
	    if (wildsok == 0 && (index(ipseg[i], '*') || index(ipseg[i], '?'))) {
		retval = FALSE;
	    }
	    MyFree(ipseg[i]);
	}


    }

    return (retval);
#undef userhost
#undef IP_WILDS_OK

}

int DelGZline(char *userhost, char *username)
{
    Reg1 aGZline *gzptr;
    Reg2 aGZline *gzptr2;

    if (GZlineList == NULL)
	return 0;

    for (gzptr = GZlineList; gzptr; gzptr = gzptr2) {

	gzptr2 = gzptr->next;

	if (!strcasecmp(gzptr->ip, userhost) && !match(gzptr->ident, username)) {
	    if (gzptr->prev)
		gzptr->prev->next = gzptr->next;
	    else
		GZlineList = gzptr->next;
	    if (gzptr->next)
		gzptr->next->prev = gzptr->prev;
	    free_gzline(gzptr);
	    return 1;
	}
    }

    return 0;
}

int GZlineExists(char *userhost, char *username)
{
    Reg1 aGZline *gzptr;
    Reg2 aGZline *gzptr2;

    if (GZlineList == NULL)
	return 0;

    for (gzptr = GZlineList; gzptr; gzptr = gzptr2) {

	gzptr2 = gzptr->next;

	if (!strcasecmp(gzptr->ip, userhost) && !strcasecmp(gzptr->ident, username)) {
	    return 1;
	}
    }

    return 0;
}

int find_gzlines(aClient * cptr)
{
    Reg1 aGZline *gzptr;
    Reg2 aGZline *gzptr2;

    char *username;

    username = cptr->username;

    if (GZlineList == NULL)
	return 0;

    for (gzptr = GZlineList; gzptr; gzptr = gzptr2) {

	gzptr2 = gzptr->next;

	if (!match(gzptr->ip, inetntoa((char *) &cptr->ip)) && (!match(gzptr->ident, username) || !strcasecmp(username, "unknown"))) {
	    sendto_one(cptr, "*** You are banned from this network by %s for %s.", gzptr->from, gzptr->reason);
	    sendto_one(cptr, "*** This ban will last until %s.", myctime(gzptr->duration));
	    sendto_one(cptr, "*** Please send an e-mail to " netwide_kline " for more information.");
	    sendto_ops("GZ-line active for %s!%s@%s", cptr->name, cptr->username, cptr->user->host);
	    return 1;
	}
    }

    return 0;
}

void AddGZline(char *ip, char *ident, char *reason, char *from, int flag, int duration, int timeset)
{
    Reg1 aGZline *gzptr;

    gzptr = make_gzline();

    strncpyzt(gzptr->ip, ip, sizeof(gzptr->ip));
    strncpyzt(gzptr->ident, ident, sizeof(gzptr->ident));
    strncpyzt(gzptr->reason, reason, sizeof(gzptr->reason));
    strncpyzt(gzptr->from, from, sizeof(gzptr->from));
    gzptr->flag = flag;
    gzptr->duration = duration;
    gzptr->timeset = timeset;

    if (GZlineList == NULL)
	GZlineList = gzptr;
    else {
	GZlineList->prev = gzptr;
	gzptr->next = GZlineList;
	GZlineList = gzptr;
    }

    return;
}

/*
   ** m_gzline
   **      parv[0] = sender prefix
   **      parv[1] = mask ([ident]@ip)
   **      parv[2] = reason
   **      parv[3] = duration
   **
 */
int m_gzline(cptr, sptr, parc, parv)
    aClient *cptr, *sptr;
    int parc;
    char *parv[];
{
    aClient *acptr;
    char userhost[512 + 2] = "", *in;
    char ident[20] = "";
    char *mask, *reason, *from;
    int now, timeset, duration, i, j;
    time_t t1;

    mask = reason = from = NULL;

    now = time(&t1);

    /* This part handles GZ-lines from servers */

    if (IsServer(sptr)) {
	return 0;
    }
    /* This part handles GZ-lines from IRC Operators */

    if (!IsSAdmin(sptr)) {
	sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
	return -1;
    }
    if (parc < 4) {
	sendto_one(sptr, err_str(ERR_NEEDMOREPARAMS),
		   me.name, parv[0], "GZLINE");
	return -1;
    }
    if (find_services()) {
	sendto_one(sptr, err_str(ERR_SERVICESUP), me.name, parv[0]);
	return -1;
    }
    timeset = now;
    from = parv[0];
    mask = parv[1];
    reason = parv[2];
    duration = now + atoi(parv[3]);

    /* Check for ident and valid IP address */

    if ((in = index(parv[1], '@')) && (*(in + 1) != '\0')) {
	strcpy(userhost, in + 1);
	in = &userhost[0];
	while (*in) {
	    if (!isdigit(*in) && !ispunct(*in)) {
		sendto_one(sptr, ":%s NOTICE %s :GZ-lines work only with ip addresses.", me.name, sptr->name);
		return -1;
	    }
	    in++;
	}

	j = 0;

	while ((mask[j] != '@')) {
	    j++;
	}

	j++;

	strncpyzt(ident, mask, j);

    } else if (in && !(*(in + 1))) {
	sendto_one(sptr, ":%s NOTICE %s :Hello?! GZ-lines need an ip address!",
		   me.name, sptr->name);
	return -1;
    } else {
	strcpy(userhost, parv[1]);
	in = &userhost[0];
	while (*in) {
	    if (!isdigit(*in) && !ispunct(*in)) {
		sendto_one(sptr, ":%s NOTICE %s :GZ-lines work only with ip addresses.", me.name, sptr->name);
		return -1;
	    }
	    in++;
	    strncpyzt(ident, "*", 2);
	}
    }

    if (advanced_check(userhost, TRUE) == FALSE) {
	sendto_ops("Bad GZ-line mask from %s *@%s [%s]", parv[0], userhost, reason ? reason : "");
	if (MyClient(sptr))
	    sendto_one(sptr, ":%s NOTICE %s :*@%s is a bad GZ-line mask...", me.name, sptr->name, userhost);
	return -1;
    }
    if (GZlineExists(userhost, ident)) {
	if (MyClient(sptr))
	    sendto_one(sptr, ":%s NOTICE %s :*@%s already exists in my GZ-line list.", me.name, sptr->name, userhost);
	return -1;
    }
    if ((duration - timeset) < 3600)
	duration = timeset + 3600;

    sendto_ops("%s added GZ-line for %s@%s (%s) for %i seconds", from, ident, userhost, reason, (duration - timeset));

    AddGZline(userhost, ident, reason, from, 2, duration, timeset);

    for (i = highest_fd; i > 0; i--) {

	if ((acptr = local[i])) {

	    if (IsLog(acptr) || IsMe(acptr)) {
		continue;
	    }
	    if (find_gzlines(acptr)) {
		return exit_client(acptr, acptr, &me, "GZ-Lined");
	    }
	}
    }

    sendto_serv_butone(":%s GZLINE %s %s %s", me.name, parv[1], parv[2], parv[3]);

    return 0;
}

/*
   ** m_ungzline
   **      parv[0] = sender prefix
   **      parv[1] = mask ([ident]@ip)
 */
int m_ungzline(cptr, sptr, parc, parv)
    aClient *cptr, *sptr;
    int parc;
    char *parv[];
{

    char userhost[512 + 2] = "", *in;
    char ident[20] = "";
    char *mask, *from;
    int j;

    if (!IsSAdmin(sptr)) {
	sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
	return -1;
    }
    if (parc < 2) {
	sendto_one(sptr, err_str(ERR_NEEDMOREPARAMS),
		   me.name, parv[0], "GZLINE");
	return -1;
    }
    from = parv[0];
    mask = parv[1];

    /* Check for ident and valid IP address */

    if ((in = index(parv[1], '@')) && (*(in + 1) != '\0')) {
	strcpy(userhost, in + 1);
	in = &userhost[0];
	while (*in) {
	    if (!isdigit(*in) && !ispunct(*in)) {
		sendto_one(sptr, ":%s NOTICE %s :GZ-lines work only with ip addresses.", me.name, sptr->name);
		return -1;
	    }
	    in++;
	}

	j = 0;

	while ((mask[j] != '@')) {
	    j++;
	}

	j++;

	strncpyzt(ident, mask, j);

    } else if (in && !(*(in + 1))) {
	sendto_one(sptr, ":%s NOTICE %s :Hello?! GZ-lines need an ip address!",
		   me.name, sptr->name);
	return -1;
    } else {
	strcpy(userhost, parv[1]);
	in = &userhost[0];
	while (*in) {
	    if (!isdigit(*in) && !ispunct(*in)) {
		sendto_one(sptr, ":%s NOTICE %s :GZ-lines work only with ip addresses.", me.name, sptr->name);
		return -1;
	    }
	    in++;
	    strncpyzt(ident, "*", 2);
	}
    }

    if (!DelGZline(userhost, ident)) {
	if (MyClient(sptr))
	    sendto_one(sptr, ":%s NOTICE %s :*@%s does not exists in my GZ-line list.", me.name, sptr->name, userhost);
	return -1;
    }
    sendto_ops("%s deleted GZ-line for %s@%s", from, ident, userhost);

    return 1;
}
