/*   PTlink ircd - PTlink Internet Relay Chat daemon, 
**   src/s_conf_edit.c
**
**** Ported from:
**   Written for UltimateIRCd by ShadowRealm Creations.
**   Copyright (C) 1997-1999 Infomedia Inc.
****
**   Adapted and optimized to PTlink ircd by Lamego@PTlink.net
**   Copyright (C) 1999-2000 PTlink Coders Team
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation; either version 1, or (at your option)
**   any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include "struct.h"
#include "common.h"
#include "numeric.h"
#include "msg.h"
#include "h.h"

/*
 * m_addcnline
 * m_addcnline - This function adds c/n lines to the ircd.conf
 *  parv[0] = sender
 *  parv[1] = server hostname/ip
 *  parv[2] = C/N password (does not support encrypted C: passwords)
 *  parv[3] = server name
 *  parv[4] = connection port
 *  parv[5] = connection class
 */
int m_addcnline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
    FILE *fp;	
    char usrstr[300];
    time_t cur = time (NULL);

    if (!IsAdmin(sptr) || !MyClient (sptr)) {
	sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
	return -1;
    }
    if (parc < 6) {
	sendto_one (sptr, ":%s NOTICE %s :Help on ADDCNLINE", me.name, parv[0]);
	sendto_one(sptr,
               ":%s NOTICE %s :Syntax: /ADDCNLINE <ip/hostname> <password>"
               " <servername> <port|*> <class|40>", me.name, parv[0]);
	return -1;
    }
    
    sendto_one(sptr, "NOTICE %s :Adding C/N lines", parv[0]);
    fp = fopen(configfile, "a");
    fprintf(fp, "#CNLines# Added by %s (%s@%s) on %s",
    parv[0], sptr->user->username, sptr->sockhost, ctime (&cur));
    sprintf(usrstr, "C:%s:%s:%s:%s:%s\n", parv[1], parv[2], parv[3],
		parv[4], parv[5]);
    fprintf(fp, "%s", usrstr);
    fprintf(fp, "N:%s:%s:%s:*:%s\n", parv[1], parv[2], parv[3], parv[5]);
    fclose(fp);
    /* should always notice change, even if rehash fails - Lamego */
    sendto_one (sptr, ":%s NOTICE %s :C/N now active for %s (%s) Class %s",
         me.name, parv[0], parv[1], parv[3], parv[5]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s has added an active C/N line set for %s (%s) Class %s", 
	me.name, parv[0], parv[1], parv[3], parv[5]);
    sendto_failops("from %s : %s has added an active C/N line set for %s (%s) Class %s", 
        me.name, parv[0], parv[1], parv[3], parv[5]);
    if (rehash (cptr, sptr, 0)) {
      sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!",
                  me.name, parv[0]);
      return -1;
    }
    return 0;
}

/*
**
** m_delcnline
** parv[0] = sender
** parv[1] = server name
*/
int m_delcnline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
    aConfItem *aconf;
    FILE *in, *out;
    char buf[256], temppath[256];
    sprintf(temppath, "%s.tmp", configfile);


    if (!IsAdmin(sptr) || !MyClient (sptr)) {
	sendto_one (sptr, err_str (ERR_NOPRIVILEGES), me.name, parv[0]);
	return -1;
    }
    if (parc < 2) {
	sendto_one (sptr, ":%s NOTICE %s :Help on DELCNLINE", me.name, parv[0]);
	sendto_one (sptr, ":%s NOTICE %s :Syntax /DELCNLINE <servername>", me.name,
                  parv[0]);
	return -1;
    }
    for (aconf = conf; aconf; aconf = aconf->next)
	if (aconf->status == CONF_CONNECT_SERVER && !match(parv[1], aconf->name))
	    break;

    if (!aconf) {
      sendto_one(sptr, ":%s NOTICE %s :DELCNLINE: server name %s not listed in %s",
                  me.name, parv[0], parv[1], configfile);
      return -1;
    }
    
    in = fopen (configfile, "r");
    out = fopen (temppath, "w");
    while (fgets (buf, sizeof (buf), in)) {
    if (!strstr (buf, parv[1]) 
    || (buf[0]!='C' && buf[0]!='N')) /* just clear C/N lines - Lamego */
	fputs (buf, out);
    }
    fclose (in);
    fclose (out);
    if (rename (temppath, configfile) == -1)
    {
	sendto_one (sptr, ":%s NOTICE %s :ERROR: rename() failed!", me.name, parv[0]);
	return -1;
    }
    /* should always notice change, even if rehash fails - Lamego */
    sendto_one (sptr, "NOTICE %s :%s has been removed",
         parv[0], parv[1]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s removed C/N line set for %s", 
	me.name, parv[0], parv[1]);
    sendto_failops("from %s : %s removed C/N line set for %s", 
	me.name, parv[0], parv[1]);
	
    if (rehash (cptr, sptr, 0)) {
      sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!",
                  me.name, parv[0]);
      return -1;
    }
  return 0;
}

/*
**
** m_addhline
** parv[0] = sender
** parv[1] = server
*/

int m_addhline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
  FILE *fp;
  char usrstr[300];
  time_t cur = time (NULL);

  if (!IsAdmin(sptr) || !MyClient (sptr))
    {
      sendto_one (sptr, err_str (ERR_NOPRIVILEGES), me.name, parv[0]);
      return -1;
    }

  if (parc < 2)
    {
      sendto_one (sptr, ":%s NOTICE %s :Help on ADDHLINE", me.name, parv[0]);
      sendto_one (sptr, ":%s NOTICE %s :Syntax: /ADDHLINE <server>", me.name,
                  parv[0]);
      return -1;
    }

  sendto_one (sptr, "NOTICE %s :Adding H-line for %s", parv[0], parv[1]);
  fp = fopen (configfile, "a");
  fprintf (fp, "#H-Line# Added by %s (%s@%s) on %s",
           parv[0], sptr->user->username, sptr->sockhost, ctime (&cur));
  sprintf(usrstr, "H:*::%s\n", parv[1]);
  fprintf (fp, "%s", usrstr);
  fclose (fp);
    /* should always notice change, even if rehash fails - Lamego */  
    sendto_one (sptr, ":%s NOTICE %s :H-line added for %s",
              me.name, parv[0], parv[1]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s has added an active H-line for %s",
                     me.name, parv[0], parv[1]);
    sendto_failops("from %s : %s has added an active H-line for %s",
		     me.name, parv[0], parv[1]);
    if (rehash (cptr, sptr, 0)) {
	sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
	return -1;
    }
  return 0;
}


/*
**
** m_delhline
** parv[0] = sender
** parv[1] = server
**
*/
int m_delhline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
  aConfItem *aconf;
  FILE *in, *out;
  char buf[256], temppath[256];

  sprintf(temppath, "%s.tmp", configfile);


  if (!IsAdmin (sptr) || !MyClient (sptr))
    {
      sendto_one (sptr, err_str (ERR_NOPRIVILEGES), me.name, parv[0]);
      return -1;
    }

  if (parc < 2)
    {
      sendto_one (sptr, ":%s NOTICE %s :Help on DELHLINE", me.name, parv[0]);
      sendto_one (sptr, ":%s NOTICE %s :Syntax /DELHLINE <server>", me.name,
                  parv[0]);
      return -1;
    }
    for (aconf = conf; aconf; aconf = aconf->next)
	if (aconf->status == CONF_HUB && !match(parv[1], aconf->name))
	    break;
  
  if (!aconf) {
      sendto_one (sptr, ":%s NOTICE %s :DELHLINE: Specified server %s not listed"
                        " in %s",
                  me.name, parv[0], parv[1], configfile);
      return -1;
    }
  in = fopen (configfile, "r");
  out = fopen (temppath, "w");
  while (fgets (buf, sizeof (buf), in))
    if (!strstr (buf, parv[1]) || buf[0]!='H') /* only remove H:line - Lamego*/
      fputs (buf, out);

  fclose (in);
  fclose (out);
  if (rename (temppath, configfile) == -1)
    {
      sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
      return -1;
    }
    /* should always notice change, even if rehash fails - Lamego */      
    sendto_one (sptr, "NOTICE %s :H-Line for %s has been removed",
		    parv[0], parv[1]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s have removed H-line for %s", 
		    me.name, parv[0], parv[1]);

    sendto_failops("from %s : %s have removed H-line for %s", 
		    me.name, parv[0], parv[1]);

    if (rehash (cptr, sptr, 0)) {
	sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
	return -1;
    }
  return 0;
}

/*
**
** m_adduline
** parv[0] = sender
** parv[1] = server
**
*/

int m_adduline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
  FILE *fp;
  char usrstr[300];
  time_t cur = time (NULL);


  if (!IsAdmin(sptr) || !MyClient (sptr))
    {
      sendto_one (sptr, err_str (ERR_NOPRIVILEGES), me.name, parv[0]);
      return -1;
    }

  if (parc < 2)
    {
      sendto_one (sptr, ":%s NOTICE %s :Help on ADDULINE", me.name, parv[0]);
      sendto_one (sptr, ":%s NOTICE %s :Syntax: /ADDULINE <server>", me.name,
                  parv[0]);
      return -1;
    }

  sendto_one (sptr, "NOTICE %s :Adding U-line for %s", parv[0], parv[1]);
  fp = fopen (configfile, "a");
  fprintf (fp, "#U-Line# Added by %s (%s@%s) on %s",
           parv[0], sptr->user->username, sptr->sockhost, ctime (&cur));
  sprintf(usrstr, "H:%s:*:*\n", parv[1]);
  fprintf (fp, "%s", usrstr);
  fclose (fp);
    /* should always notice change, even if rehash fails - Lamego */  
    sendto_one (sptr, ":%s NOTICE %s :U-line added for %s",
              me.name, parv[0], parv[1]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s has added an active H-line for %s",
                     me.name, parv[0], parv[1]);
    sendto_failops("from %s : %s has added an active U-line for %s",
		     me.name, parv[0], parv[1]);
    if (rehash (cptr, sptr, 0)) {
	sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
	return -1;
    }
  return 0;
}


/*
**
** m_deluline
** parv[0] = sender
** parv[1] = server
**
*/
int m_deluline(aClient *cptr, aClient *sptr, int parc, char **parv)
{
  aConfItem *aconf;
  FILE *in, *out;
  char buf[256], temppath[256];

  sprintf(temppath, "%s.tmp", configfile);

  if (!IsAdmin (sptr) || !MyClient (sptr))
    {
      sendto_one (sptr, err_str (ERR_NOPRIVILEGES), me.name, parv[0]);
      return -1;
    }

  if (parc < 2)
    {
      sendto_one (sptr, ":%s NOTICE %s :Help on DELULINE", me.name, parv[0]);
      sendto_one (sptr, ":%s NOTICE %s :Syntax /DELULINE <server>", me.name,
                  parv[0]);
      return -1;
    }
    for (aconf = conf; aconf; aconf = aconf->next)
	if (aconf->status == CONF_HUB && !match(parv[1], aconf->name))
	    break;
  
  if (!aconf) {
      sendto_one (sptr, ":%s NOTICE %s :DELULINE: Specified server %s not listed"
                        " in %s",
                  me.name, parv[0], parv[1], configfile);
      return -1;
    }
  in = fopen (configfile, "r");
  out = fopen (temppath, "w");
  while (fgets (buf, sizeof (buf), in))
    if (!strstr (buf, parv[1]) || buf[0]!='U') /* only remove U:line - Lamego*/
      fputs (buf, out);

  fclose (in);
  fclose (out);
  if (rename (temppath, configfile) == -1)
    {
sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
      return -1;
    }
    /* should always notice change, even if rehash fails - Lamego */      
    sendto_one (sptr, "NOTICE %s :U-Line for %s has been removed",
		    parv[0], parv[1]);
    sendto_serv_butone(&me, ":%s GNOTICE :%s have removed U-line for %s", 
		    me.name, parv[0], parv[1]);

    sendto_failops("from %s : %s have removed U-line for %s", 
		    me.name, parv[0], parv[1]);

    if (rehash (cptr, sptr, 0)) {
	sendto_one (sptr, ":%s NOTICE %s :ERROR: rehash() failed!", me.name, parv[0]);
	return -1;
    }
  return 0;
}

/*
** m_display
**      parv[0] = sender prefix
**      parv[1] = filename
**	parv[2] = servername (netadmins only)
*/
int     m_display(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
{
        int     fd, nr;
        char    line[80];
        Reg1    char     *tmp;
        struct  stat    sb;

        if (!IsAdmin(sptr)) {
    	    sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
        return 0; }

        if (parc < 2)
        {
                sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
                            me.name, parv[0], "DISPLAY");
                return 0;
        }
/* for security sake - Lamego
	if (IsNetAdmin(sptr)) {
        if (hunt_server(cptr, sptr, ":%s DISPLAY :%s", 2,parc,parv)!=HUNTED_ISME)
                return 0;
        }
*/
        /*
         * stop NFS hangs...most systems should be able to open a file in
         * 3 seconds. -avalon (curtesy of wumpus)
         */

	if (strstr(parv[1], "/")) { 
	sendto_one(sptr, ":%s NOTICE %s :*** Sorry, only files in the current directory.", me.name, parv[0]);
		return 0; }

        fd = open(parv[1], O_RDONLY);
        if (fd == -1)
            {
		sendto_one(sptr, ":%s NOTICE %s :*** Sorry, could not open [%s] - Not enough access or the file does not exist.", me.name, parv[0], parv[1]);
                return 0;
            }
        (void)fstat(fd, &sb);
        sendto_umode(UMODE_OPER,"*** %s (%s@%s) displayed file: \2%s\2", parv[0],
    	    sptr->user->username, sptr->user->host, parv[1]);
	sendto_one(sptr, ":%s NOTICE %s :-- DISPLAYING [\2%s\2] --", me.name, parv[0], parv[1]);
        (void)dgets(-1, NULL, 0); /* make sure buffer is at empty pos */
        while ((nr=dgets(fd, line, sizeof(line)-1)) > 0)
            {
                line[nr]='\0';
                if ((tmp = (char *)index(line,'\n')))
                        *tmp = '\0';
                if ((tmp = (char *)index(line,'\r')))
                        *tmp = '\0';
        sendto_one(sptr, ":%s NOTICE %s :%s", me.name, parv[0], line);
            }
        (void)dgets(-1, NULL, 0); /* make sure buffer is at empty pos */
	sendto_one(sptr, ":%s NOTICE %s :-- CLOSING [\2%s\2] -- ", me.name, parv[0], parv[1]);
        (void)close(fd);
        return 0;
    }

/*
** m_write
**      parv[0] = sender prefix
**      parv[1] = filename
**      parv[2] = servername (netadmins only)
**      parv[3] = stuff to write
*/
int     m_write(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
{
        int     i;
	FILE	*fd;

        if (!IsAdmin(sptr)) {
    	    sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
        return 0; }
/* for security sake - Lamego
        if (IsNetAdmin(sptr)) {

        if (hunt_server(cptr, sptr, ":%s WRITE %s %s :%s", 2,parc,parv)!=HUNTED_ISME)
                return 0;

        if (parc < 4)
        {
                sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
                            me.name, parv[0], "WRITE");
                return 0;
        }

        *
         * stop NFS hangs...most systems should be able to open a file in
         * 3 seconds. -avalon (curtesy of wumpus)
         *

        if (strstr(parv[1], "/")) {
        sendto_one(sptr, ":%s NOTICE %s :*** Sorry, only in the current directory.", me.name, parv[0]);
                return 0; }

        fd = fopen(parv[1], "a");
        if (fd == NULL)
            {
                sendto_one(sptr, ":%s NOTICE %s :*** Sorry, could not open [%s] - Not enough access or the file does not exist.", me.name, parv[0], parv[1]);
                return 0;
            }
        for (i=3; i<parc; i++)
	{
		if (i!=parc-1)
        fprintf (fd, "%s ", parv[i]);
		else
	fprintf (fd, "%s\n", parv[i]);
	}
        sendto_one(sptr, ":%s NOTICE %s :*** Wrote to file: \2%s\2, server: \2%s\2", me.name, parv[0], parv[1], parv[2]);
        sendto_umode(UMODE_OPER,"*** %s (%s@%s) wrote to file: \2%s\2", parv[0],
        sptr->user->username, sptr->user->host, parv[1]);
	} else */ {
        if (parc < 3)
        {
                sendto_one (sptr, err_str(ERR_NEEDMOREPARAMS),
                            me.name, parv[0], "WRITE");
                return 0;
        }

        /*
         * stop NFS hangs...most systems should be able to open a file in
         * 3 seconds. -avalon (curtesy of wumpus)
         */

        if (strstr(parv[1], "/")) {
        sendto_one(sptr, ":%s NOTICE %s :*** Sorry, only in the current directory.", me.name, parv[0]);
                return 0; }

        fd = fopen(parv[1], "a");
        if (fd == NULL)
            {
                sendto_one(sptr, ":%s NOTICE %s :*** Sorry, could not open [%s] - Not enough access or the file does not exist.", me.name, parv[0], parv[1]);
                return 0;
            }

        for (i=2; i<parc; i++)
	{
                if (i!=parc-1)
        fprintf (fd, "%s ", parv[i]);
                else
        fprintf (fd, "%s\n", parv[i]);
	}
        sendto_one(sptr, ":%s NOTICE %s :*** Wrote to file: \2%s\2", me.name, parv[0], parv[1]);
	sendto_umode(UMODE_OPER,"*** %s (%s@%s) wrote to file: \2%s\2", parv[0],
	sptr->user->username, sptr->user->host, parv[1]);
	}
	fclose (fd);
        return 0;
    }
    
/*
** m_mkpasswd
**	parv[0] = sender prefix
**	parv[1] = password to encrypt
*/
int     m_mkpasswd(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
    {
#ifdef CRYPT_OPER_PASSWORD    
  static char saltChars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";
  char salt[3];
  extern  char *crypt();
  int useable = 0;

  if (parc > 1)
	{
		if (strlen(parv[1]) >= 1)
			useable = 1;
	}

  if (useable == 0)
	{
		sendto_one(sptr, ":%s NOTICE %s :*** Encryption's MUST be atleast 1 character in length", me.name, parv[0]);
                return 0;
	}
    srandom(time(0));           
    salt[0] = saltChars[random() % 64];
    salt[1] = saltChars[random() % 64];
    salt[2] = 0;

    if ((strchr(saltChars, salt[0]) == NULL) || (strchr(saltChars, salt[1]) == NULL))
	{
	sendto_one(sptr, ":%s NOTICE %s :*** Illegal salt %s", me.name, parv[0], salt);
		return 0;
	}


  sendto_one(sptr, ":%s NOTICE %s :*** Encryption for [%s] is %s", me.name, parv[0], parv[1], crypt(parv[1], salt));
  #endif
  return 0;
}
