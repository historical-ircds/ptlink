/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_socks.c
 *   Copyright (C) 1998	Lucas Madar
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* All these includes are copied from auth.c because we are
   just doing something very similar */
#include "netcfg.h"
#include "config.h"		/* Suck in the value of SOCKSPORT */

#ifdef CHECKFORSOCKS

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "res.h"
#include "numeric.h"
#include "patchlevel.h"
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#ifdef	UNIXPORT
#include <sys/un.h>
#endif
#if defined(__hpux)
#include "inet.h"
#endif
#include <fcntl.h>
#include "sock.h"		/* If FD_ZERO isn't define up to this point,  */
			/* define it (BSD4.2 needs this) */
#include "h.h"

static unsigned char socksid[12];
#define HICHAR(s)     (((unsigned short) s) >> 8)
#define LOCHAR(s)     (((unsigned short) s) & 0xFF)

/* init_socks
 * Set up socksid, simply.
 */

void init_socks(aClient * cptr)
{
    unsigned short sport = SOCKSPORT;
    struct sockaddr_in sin;

    socksid[0] = 4;
    socksid[1] = 1;
    socksid[2] = HICHAR(sport);
    socksid[3] = LOCHAR(sport);
    socksid[8] = 0;

    if ((cptr->socksfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	return;

    set_non_blocking(cptr->socksfd, cptr);

    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(sport);
    sin.sin_family = AF_INET;

    if (bind(cptr->socksfd, (struct sockaddr *) &sin, sizeof(sin))) {
	close(cptr->socksfd);
	cptr->socksfd = -1;
	return;
    }
    listen(cptr->socksfd, LISTEN_SIZE);

    /* Socks lietening port is now set up */
    /* Similar to discard port */
}

/*
 * start_socks
 *
 * attempts to connect to a socks server on port 1080 of the host.
 * if the connect fails, the user passes the socks check
 * otherwise, the connection is checked to see if it is a "secure"
 * socks4+ server
 */
void start_socks(cptr)
    aClient *cptr;
{
    struct sockaddr_in sin;
    int sinlen = sizeof(struct sockaddr_in);
    if (find_socksexcept((char *)inetntoa((char *) &cptr->ip))) {
        write(cptr->fd, REPORT_SOCKSEXCEPT, R_socksexcept);
        return;
    }
    if ((cptr->socksfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
	Debug((DEBUG_ERROR, "Unable to create socks socket for %s:%s",
	       get_client_name(cptr, TRUE),
	       strerror(get_sockerr(cptr))));
	return;
    }
    if (cptr->socksfd >= (MAXCONNECTIONS - 3)) {
	sendto_ops("Can't allocate fd for socks on %s",
		   get_client_name(cptr, TRUE));
	close(cptr->socksfd);
	cptr->socksfd = -1;
	return;
    }
#ifdef SHOWCONNECTINFO
    write(cptr->fd, REPORT_DO_SOCKS, R_do_socks);
#endif

    set_non_blocking(cptr->socksfd, cptr);

    sin.sin_port = htons(1080);
    sin.sin_family = AF_INET;
    bcopy((char *) &cptr->ip, (char *) &sin.sin_addr,
	  sizeof(struct in_addr));

    if (connect(cptr->socksfd, (struct sockaddr *) &sin,
		sinlen) == -1 && errno != EINPROGRESS) {
	/* we have no socks server! */
	close(cptr->socksfd);
	cptr->socksfd = -1;
#ifdef SHOWCONNECTINFO
	write(cptr->fd, REPORT_NO_SOCKS, R_no_socks);
#endif
	return;
    }
    cptr->flags |= (FLAGS_WRSOCKS | FLAGS_SOCKS);
    if (cptr->socksfd > highest_fd)
	highest_fd = cptr->socksfd;
    return;
}

/*
 * send_socksquery
 *
 * send the socks server a query to see if it's open.
 */
void send_socksquery(cptr)
    aClient *cptr;
{
    struct sockaddr_in sin;
    int sinlen = sizeof(struct sockaddr_in);
    unsigned char socksbuf[12];
    unsigned long theip;

    bcopy((char *) &socksid, (char *) &socksbuf, 9);

    getsockname(cptr->fd, (struct sockaddr *) &sin, &sinlen);

    theip = htonl(sin.sin_addr.s_addr);

    socksbuf[4] = (theip >> 24);
    socksbuf[5] = (theip >> 16) & 0xFF;
    socksbuf[6] = (theip >> 8) & 0xFF;
    socksbuf[7] = theip & 0xFF;

    if (send(cptr->socksfd, socksbuf, 9, 0) != 9) {
	close(cptr->socksfd);
	if (cptr->socksfd == highest_fd)
	    while (!local[highest_fd])
		highest_fd--;
	cptr->socksfd = -1;
	cptr->flags &= ~FLAGS_SOCKS;
#ifdef SHOWCONNECTINFO
	write(cptr->fd, REPORT_NO_SOCKS, R_no_socks);
#endif
    }
    cptr->flags &= ~FLAGS_WRSOCKS;
    return;
}

/*
 * read_socks
 *
 * process the socks reply.
 */

void read_socks(cptr)
    aClient *cptr;
{
    unsigned char socksbuf[12];
    int len;

    len = recv(cptr->socksfd, socksbuf, 9, 0);

    (void) close(cptr->socksfd);
    if (cptr->socksfd == highest_fd)
	while (!local[highest_fd])
	    highest_fd--;
    cptr->socksfd = -1;
    ClearSocks(cptr);

    if (len < 4) {
#ifdef SHOWCONNECTINFO
	write(cptr->fd, REPORT_NO_SOCKS, R_no_socks);
#endif
	return;
    }
    if (socksbuf[1] == 90) {
	cptr->flags |= FLAGS_GOTSOCKS;
	return;
    }
#ifdef SHOWCONNECTINFO
    write(cptr->fd, REPORT_GOOD_SOCKS, R_good_socks);
#endif
    return;
}

#endif				/* CHECKFORSOCKS */
