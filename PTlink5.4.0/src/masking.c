/************************************************************************
 *   IRC - Internet Relay Chat, src/masking.c
 *   Copyright (C) 1998	Dean F. Bailey
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "netcfg.h"
#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "msg.h"
#include "channel.h"
#include "userload.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <utmp.h>
#include "h.h"

int tagcount = 0;
#define MAXVIRTSIZE     (3 + 5 + 1)
#define HASHVAL_TOTAL   30011
#define HASHVAL_PARTIAL 211

char *hideme(acptr)
    aClient *acptr;
{

    if (!IsRegistered(acptr)) {
	return "<Unknown>";
    }
    if(IsAnOper(acptr)|| IsHelpOp(acptr))    
	SetNewHost(acptr); else ClearNewHost(acptr);
    if (IsHidden(acptr)) {
	if (IsNetAdmin(acptr))
	    return netadmin_host;
	else if (IsTAdmin(acptr))
	    return tadmin_host;
	if (IsSAdmin(acptr))
	    return sadmin_host;
	else if (IsAdmin(acptr))
	    return admin_host;
	else if (IsOper(acptr))
	    return oper_host;
	else if (IsLocOp(acptr))
	    return locop_host;
	else if (IsHelpOp(acptr))
	    return helper_host;	    
	if (*acptr->user->host && (!MyConnect(acptr) || IsUnixSocket(acptr)))
	    return maskme(acptr->user->host);
	else
	    return maskme(acptr->sockhost);
    } else {
	if (*acptr->user->host && (!MyConnect(acptr) || IsUnixSocket(acptr)))
	    return acptr->user->host;
	else
	    return acptr->sockhost;
    }
}
char *maskmenow(acptr, sptr)
    aClient *acptr, *sptr;
{
    if (*acptr->user->host && (!MyConnect(acptr) || IsUnixSocket(acptr)))
	return maskme(acptr->user->host);
    else
	return maskme(acptr->sockhost);
}

#define B_BASE                  0x1000

void truncstring(char *stringvar, int firstlast, int amount){
   if (firstlast)
   {
    stringvar+=amount;
    *stringvar=0;
    stringvar-=amount;
   }
    else
   {
    stringvar+=strlen(stringvar);
    stringvar-=amount;
   }
}

int Maskchecksum (char *data, int len)
{
        int                     i;
        int                     j;

        j=0;
        for (i=0 ; i<len ; i++)
        {
          j += *data++ * (i < 16 ? (i+1)*(i+1) : i*(i-15));

        }

        return (j+1000)%0xffff;
}

unsigned z(char *s)
{
    unsigned int i;
        i = Maskchecksum (s, strlen(s));
    return i;
}

int str2array(char **pparv, char *string, char *delim)
{
    char *tok;
    int pparc = 0;

    tok = (char *)strtok(string, delim);
    while (tok != NULL) {
	pparv[pparc++] = tok;
	tok = (char *)strtok(NULL, delim);
    }

    return pparc;
}


char *maskme(char *curr)
{
    static char mask[HOSTLEN + 1];
    unsigned hash_total = z(curr) % HASHVAL_TOTAL, hash_partial;
    char     destroy[HOSTLEN+1], *parv[HOSTLEN+1], *ptr;
    int      parc=0, len=0, overflow=0, exception=0;

    strncpyzt(destroy, curr, HOSTLEN);

    len = strlen(destroy);

    if ((len + MAXVIRTSIZE) > HOSTLEN)
    {
        overflow = (len + MAXVIRTSIZE) - HOSTLEN;

        ptr = &destroy[overflow];
    }
    else
        ptr = &destroy[0];

    parc = str2array(parv, ptr, ".");

    if (parc == 4)
    {
        len = strlen(parv[3]);

        if (strchr("0123456789", parv[3][len-1]))
        {
            hash_partial = (z(parv[0])+z(parv[1])+z(parv[2])) % HASHVAL_PARTIAL;
            sprintf(mask, "%s.%s.%u.%s-%u",
                parv[0], parv[1], hash_partial, x_prefix, hash_total);
            return mask;
        }
    }

    if (parc == 2)
    {
        sprintf(mask, "%s-%u.%s.%s",
            x_prefix, hash_total, parv[parc-2], parv[parc-1]);

        return mask;
    }

    len = strlen(parv[parc-1]);
/* For PTlink Services compatibility
    if ((len==2) && (find_exception(parv[parc-1])))
        exception = 1;
*/
    if ((len == 2) && (exception == 0))
    {
        sprintf(mask, "%s-%u.%s.%s", /* Fixes spoofing - Lamego 1999*/
            x_prefix, hash_total, parv[parc-2], parv[parc-1]);	
        return mask;
    }
    sprintf(mask, "%s-%u.%s.%s", x_prefix, hash_total, parv[parc-2], parv[parc-1]);
    return mask;    
}

/*
    m_newhost
    parv[0] = send
    parv[1] = new masked hostname
*/

int     m_newhost(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
    {
        char    *newhost;
        char 	*s;
        int     legalhost = 1;

	if (MyClient(sptr)) {
    	    if (!IsOper(sptr)) {
    		sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
    		return -1; 
	    }
#ifndef CANUSENEWHOST
	    sendto_one(sptr, err_str(ERR_NOPRIVILEGES), me.name, parv[0]);
	    return -1;
#endif
	}
        newhost = parc > 1 ? parv[1] : NULL;

        if (BadPtr(newhost))
            {
    		sendto_one(sptr, ":%s NOTICE %s :*** Syntax: /NEWHOST <new host>", me.name, parv[0]);
                return 0;
            }

            if (strlen(parv[1]) > (HOSTLEN)) {
           sendto_one(sptr, ":%s NOTICE %s :*** Error: Hostname too long", me.name, parv[0]);
           return -1;
            }

                for (s = parv[1]; *s; s++) {
                        if (!isallowed(*s)) {
                                legalhost = 0;
                        }
                }
                if (legalhost == 0) {
                        sendto_one(sptr, ":%s NOTICE %s :*** Error: The hostname containes \
			illegal characters, please only use a-z, A-Z, 0-9, '-' & '.'", me.name, 
			parv[0]);
                        return -1;
                }


	(void)sprintf(sptr->user->mask, "%s", newhost);
	SetHidden(sptr);
	SetNewHost(sptr);
	sendto_serv_butone(cptr, ":%s NEWHOST %s", parv[0], newhost);
	if (MyClient(sptr)) 
	    sendto_one(sptr, ":%s NOTICE %s :*** New hostmask: \2%s (%s@%s)\2", me.name, parv[0],
							parv[0], sptr->user->username, parv[1]);
	return 0;	  
    }
