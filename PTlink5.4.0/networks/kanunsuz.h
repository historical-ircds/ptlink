/**********************************************************************
                Network information
 **********************************************************************/
#define irc_network     "Kanunsuz.NET" 
#define admin_chan      "#Kanunsuz" 
#define random_serv     "irc.kanunsuz.net" 
#define network_www     "http://www.kanunsuz.net" 
#define network_aup     "http://www.kanunsuz.net" 
#define netwide_kline   "kline@kanunsuz.net" 

/**********************************************************************
                        Network contacts
 **********************************************************************/
#define contact_url     "http://www.kanunsuz.net" 
#define contact_email   "admin@kanunsuz.net" 
#define kline_address   "kline@kanunsuz.net" 

/**********************************************************************
                        Host masking
 **********************************************************************/
#define x_prefix        "kanunsuz" 
#define helper_host      "Helper" 
#define locop_host      "LocOper" 
#define oper_host       "IRCop" 
#define admin_host      "Admin" 
#define sadmin_host     "SAdmin" 
#define tadmin_host     "TechAdmin" 
#define netadmin_host   "NetAdmin" 

/**********************************************************************
                    User connect auto modes
 **********************************************************************/
#define NO_DEFAULT_HIDE
#define NO_DEFAULT_INVISIBLE

/**********************************************************************
                    Clone checking / Socks Detection
 **********************************************************************/
#define THROTTLE
#define CHECK_CLONE_LIMIT   4
#define CHECK_CLONE_PERIOD 15
#define CHECK_CLONE_DELAY  60
#undef CHECKFORSOCKS

/**********************************************************************
                                Service
 **********************************************************************/
#define SERVICES_NAME   "services.kanunsuz.net" 
#define ChanServ        "ChanServ" 
#define MemoServ        "MemoServ" 
#define NickServ        "NickServ" 
#define OperServ        "OperServ" 
#define NewsServ        "NewsServ" 
#define StatServ        "StatServ" 

/**********************************************************************
                        IRC Operators privileges
 **********************************************************************/
#define CANUSENEWHOST
#undef  KICKPROTECTION
#undef NOSELFKILL
#define  NOOPERKILL
#define  CANALWAYSJOIN
#define OPERCANALWAYSSEND
#define NOCOLORQUITS
#undef HIDE_ULINE
