/**********************************************************************
  			Network information		    
 **********************************************************************/
#define irc_network 	"PTlink"
#define admin_chan  	"#PTlink"
#define random_serv 	"irc.ptlink.net"
#define network_www 	"http://www.ptlink.net"
#define network_aup 	"http://www.ptlink.net/aup/"
#define netwide_kline 	"kline@ptlink.net"

/**********************************************************************
  			Network contacts		    
 **********************************************************************/
#define contact_url 	"http://www.ptlink.net"
#define contact_email 	"admin@ptlink.net"
#define kline_address 	"kline@ptlink.net"

/**********************************************************************
  			Host masking		    
 **********************************************************************/
#define x_prefix 	"PTlink"
#define oper_host       "IRCop.PTlink.net"
#define admin_host      "Admin.PTlink.net"
#define locop_host      "Local.PTlink.net"
#define sadmin_host     "SAdmin.PTlink.net"
#define tadmin_host     "TechAdmin.PTlink.net"
#define netadmin_host   "NetAdmin.PTlink.net"
#define helper_host	"Helper.PTlink.net"

/**********************************************************************
  		    User connect auto modes
 **********************************************************************/
#undef 	NO_DEFAULT_HIDE		/* define to disable auto +i */
#undef 	NO_DEFAULT_INVISIBLE	/* define to disable auto +x */

/**********************************************************************
		    Clone checking / Socks Detection
 **********************************************************************/
#define THROTTLE	 	/* undefine to disable clone checking */
#define CHECK_CLONE_LIMIT   3	/* after n connections */
#define CHECK_CLONE_PERIOD 15	/* during time (seconds) */
#define CHECK_CLONE_DELAY  60	/* will put a temp zline (seconds) */

#define CHECKFORSOCKS		/* undefine to disable */

/**********************************************************************
				Services
 **********************************************************************/
#define SERVICES_NAME	"Services.PTlink.net"
#define ChanServ 	"ChanServ"
#define MemoServ 	"MemoServ"
#define NickServ 	"NickServ"
#define OperServ 	"OperServ"
#define NewsServ 	"NewsServ"
#define StatServ 	"StatServ"
 
/**********************************************************************
			IRC Operators privileges				
 **********************************************************************/
#define CANUSENEWHOST		/* Can use new host command */
#undef  CANUSEFMODE		/* Can use fake modes */
#define NOSELFKILL		/* Cannot do self kills */ 
#undef 	NOOPERKILL		/* Cannot kill other opers */
#undef	KICKPROTECTION		/* Cannot be kicked */
#define	OPERCANALWAYSSEND	/* Operator can always send (+b/+m) */
#undef 	CANALWAYSJOIN		/* Can always join a channel */
#undef	CANALWAYSKICK		/* Can always kick (even without @) */

/**********************************************************************
			    Misc. Settings
 **********************************************************************/
#define NOCOLORQUITS		/* Colored quit messages not allowed */
#define HIDE_ULINE		/* Hide ulined servers from /links */
