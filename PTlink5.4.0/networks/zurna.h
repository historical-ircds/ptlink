/**********************************************************************
  			Network information		    
 **********************************************************************/
#define irc_network 	"ZurnaNet"
#define admin_chan  	"#master"
#define random_serv 	"irc.zurna.net"
#define network_www 	"http://www.zurna.net"
#define network_aup 	"http://www.zurna.net/irc/"
#define netwide_kline 	"kline@zurna.net"
#define network_letters "ZN"	/* 2 chars, to be displayed on quits  */

/**********************************************************************
  			Network contacts		    
 **********************************************************************/
#define contact_url 	"http://www.zurna.net"
#define contact_email 	"ircadmin@zurna.net"
#define kline_address 	"kline@zurna.net"

/**********************************************************************
  			Host masking		    
 **********************************************************************/
#define x_prefix 	"ZurnaNet"
#define oper_host       "IRCop.zurna.net"
#define admin_host      "Admin.zurna.net"
#define locop_host      "Local.zurna.net"
#define sadmin_host     "SAdmin.zurna.net"
#define tadmin_host     "TechAdmin.zurna.net"
#define netadmin_host   "NetAdmin.zurna.net"
#define helper_host	"Helper.zurna.net"

/***********************************
  		    User connect auto modes
 **********************************************************************/
#define 	NO_DEFAULT_HIDE		/* undefine to disable auto +i */
#define 	NO_DEFAULT_INVISIBLE	/* undefine to disable auto +x */

/**********************************************************************
		    Clone checking / Socks Detection
 **********************************************************************/
#define THROTTLE	 	/* undefine to disable clone checking */
#define CHECK_CLONE_LIMIT   8	/* after n connections */
#define CHECK_CLONE_PERIOD 500	/* during time (seconds) */
#define CHECK_CLONE_DELAY  60	/* will put a temp zline (seconds) */

#define CHECKFORSOCKS		/* undefine to disable */

/**********************************************************************
				Services
 **********************************************************************/
#define SERVICES_NAME	"services.zurna.net"
#define ChanServ 	"ChanServ"
#define MemoServ 	"MemoServ"
#define NickServ 	"NickServ"
#define OperServ 	"OperServ"
#define NewsServ 	"NewsServ"
#define StatServ 	"StatServ"
 
/**********************************************************************
			IRC Operators privileges				
 **********************************************************************/
#define CANUSENEWHOST		/* Can use new host command */
#define NOSELFKILL		/* Cannot do self kills */ 
#define 	NOOPERKILL		/* Cannot kill other opers */
#undef	KICKPROTECTION		/* Cannot be kicked */
#undef	OPERCANALWAYSSEND	/* Operator can always send (+b/+m) */
#define 	CANALWAYSJOIN		/* Can always join a channel */
#undef	CANALWAYSKICK		/* Can always kick (even without @) */

/**********************************************************************
			    Misc. Settings
 **********************************************************************/
#define NOCOLORQUITS		/* Colored quit messages not allwed */
#define HIDE_ULINE		/* Hide ulined servers from /links */
