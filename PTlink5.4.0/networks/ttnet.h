/**********************************************************************
  			Network information		    
 **********************************************************************/
#define irc_network 	"TTNET"
#define admin_chan  	"#opers"
#define random_serv 	"irc.ttnet.net.tr"
#define network_www 	"http://wwwirc.ttnet.net.tr"
#define network_aup 	"http://wwwirc.ttnet.net.tr/bilgi/"
#define netwide_kline 	"ircamdin@ttnet.net.tr"

/**********************************************************************
  			Network contacts		    
 **********************************************************************/
#define contact_url 	"http://wwwirc.ttnet.net.tr"
#define contact_email 	"ircadmin@ttnet.net.tr"
#define kline_address 	"ircadmin@ttnet.net.tr"

/**********************************************************************
  			Host masking		    
 **********************************************************************/
#define x_prefix 	"TTNet"
#define oper_host       "ttnet.net.tr"
#define admin_host      "ttnet.net.tr"
#define locop_host      "ttnet.net.tr"
#define sadmin_host     "ttnet.net.tr"
#define tadmin_host     "ttnet.net.tr"
#define netadmin_host   "ttnet.net.tr"
#define helper_host	"Helpop.ttnet.net.tr"

/**********************************************************************
  		    User connect auto modes
 **********************************************************************/
#undef 	NO_DEFAULT_HIDE		/* undefine to disable auto +i */
#undef	NO_DEFAULT_INVISIBLE	/* undefine to disable auto +x */

/**********************************************************************
		    Clone checking / Socks Detection
 **********************************************************************/
#define THROTTLE	 	/* undefine to disable clone checking */
#define CHECK_CLONE_LIMIT   3	/* after n connections */
#define CHECK_CLONE_PERIOD 15	/* during time (seconds) */
#define CHECK_CLONE_DELAY  60	/* will put a temp zline (seconds) */

#define CHECKFORSOCKS		/* undefine to disable */

/**********************************************************************
				Services
 **********************************************************************/
#define SERVICES_NAME	"services.ttnet.net.tr"
#define ChanServ 	"ChanServ"
#define MemoServ 	"MemoServ"
#define NickServ 	"NickServ"
#define OperServ 	"Praetor"
#define NewsServ 	"Haberci"
#define StatServ 	"StatServ"
 
/**********************************************************************
			IRC Operators privileges				
 **********************************************************************/
#undef CANUSENEWHOST		/* Can use new host command */
#define NOSELFKILL		/* Cannot do self kills */ 
#define	NOOPERKILL		/* Cannot kill other opers */
#define	KICKPROTECTION		/* Cannot be kicked */
#define	OPERCANALWAYSSEND	/* Operator can always send (+b/+m) */
#define	CANALWAYSJOIN		/* Can always join a channel */
#define	CANALWAYSKICK		/* Can always kick (even without @) */

/**********************************************************************
			    Misc. Settings
 **********************************************************************/
#define NOCOLORQUITS		/* Colored quit messages not allwed */
#define HIDE_ULINE		/* Hide ulined servers from /links */
