/**********************************************************************
  			Network information		    
 **********************************************************************/
#define irc_network 	"bosna-chat"
#define admin_chan  	"#bosna-chat"
#define random_serv 	"irc.bosna-chat.net"
#define network_www 	"http://www.bosna-chat.net"
#define network_aup 	"http://www.bosna-chat.net"
#define netwide_kline 	"kline@bosna-chat.net"

/**********************************************************************
  			Network contacts		    
 **********************************************************************/
#define contact_url 	"http://www.bosna-chat.net"
#define contact_email 	"admin@bosna-chat.net"
#define kline_address 	"kline@bosna-chat.net"

/**********************************************************************
  			Host masking		    
 **********************************************************************/
#define x_prefix 	"bosna-chat"
#define oper_host       "ircop.bosna-chat.net"
#define admin_host      "admin.bosna-chat.net"
#define locop_host      "localop.bosna-chat.net"
#define sadmin_host     "sadmin.bosna-chat.net"
#define tadmin_host     "techadmin.bosna-chat.net"
#define netadmin_host   "netadmin.bosna-chat.net"
#define helper_host	"helper.bosna-chat.net"

/**********************************************************************
  		    User connect auto modes
 **********************************************************************/
#undef 	NO_DEFAULT_HIDE		/* undefine to disable auto +i */
#undef 	NO_DEFAULT_INVISIBLE	/* undefine to disable auto +x */

/**********************************************************************
		    Clone checking / Socks Detection
 **********************************************************************/
#define THROTTLE	 	/* undefine to disable clone checking */
#define CHECK_CLONE_LIMIT   3	/* after n connections */
#define CHECK_CLONE_PERIOD 15	/* during time (seconds) */
#define CHECK_CLONE_DELAY  60	/* will put a temp zline (seconds) */

#define CHECKFORSOCKS		/* undefine to disable */

/**********************************************************************
				Services
 **********************************************************************/
#define SERVICES_NAME	"Services.bosna-chat.net"
#define ChanServ 	"ChanServ"
#define MemoServ 	"MemoServ"
#define NickServ 	"NickServ"
#define OperServ 	"OperServ"
#define NewsServ 	"NewsServ"
#define StatServ 	"StatServ"
 
/**********************************************************************
			IRC Operators privileges				
 **********************************************************************/
#define CANUSENEWHOST		/* Can use new host command */
#define NOSELFKILL		/* Cannot do self kills */ 
#undef 	NOOPERKILL		/* Cannot kill other opers */
#undef	KICKPROTECTION		/* Cannot be kicked */
#define	OPERCANALWAYSSEND	/* Operator can always send (+b/+m) */
#undef 	CANALWAYSJOIN		/* Can always join a channel */
#undef	CANALWAYSKICK		/* Can always kick (even without @) */

/**********************************************************************
			    Misc. Settings
 **********************************************************************/
#define NOCOLORQUITS		/* Colored quit messages not allowed */
#define HIDE_ULINE		/* Hide ulined servers from /links */
