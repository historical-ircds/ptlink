/************************************************************************
 *   IRC - Internet Relay Chat, include/struct.h
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__struct_include__
#define __struct_include__

#include "config.h"	/* Get SEEUSERSTATS */
#include "common.h"
#include "sys.h"
/* #include "zlib.h" */

#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#ifdef HAVE_STDDEF_H
# include <stddef.h>
#endif

#ifdef USE_SYSLOG
# include <syslog.h>
#ifdef HAVE_SYSSYSLOG_H
#  include <sys/syslog.h>
# endif
#endif
#ifdef	pyr
#include <sys/time.h>
#endif

typedef struct  SqlineItem aSqlineItem;
typedef	struct	ConfItem aConfItem;
typedef struct	Notify	aNotify;
typedef	struct 	Client	aClient;
/* Related to SOCKS zline timer -taz */
typedef struct	Event   aEvent;
typedef struct  GZline  aGZline;
typedef struct  t_gline aGline;
typedef struct  t_vline aVline;
typedef struct  SynchList aSynchList;
typedef	struct	Channel	aChannel;
typedef	struct	User	anUser;
typedef	struct	Server	aServer;
typedef	struct	SLink	Link;
typedef struct	SBan	Ban;
typedef	struct	SMode	Mode;
typedef struct	ListOptions	LOpts;
typedef struct  CloneItem aClone;
#ifdef ZIP_LINKS
typedef struct  Zdata   aZdata;
#endif


#ifdef NEED_U_INT32_T
typedef unsigned int  u_int32_t; /* XXX Hope this works! */
#endif

#ifndef VMSP
#include "class.h"
#include "dbuf.h"	/* THIS REALLY SHOULDN'T BE HERE!!! --msa */
#endif

#define	ARGLEN		132	/* Argument length for events  -taz        */
#define	HOSTLEN		63	/* Length of hostname.  Updated to         */
				/* comply with RFC1123                     */

#define	NICKLEN		30	
				/* Necessary to put 9 here instead of 10
				** if s_msg.c/m_nick has been corrected.
				** This preserves compatibility with old
				** servers --msa
				*/
#define	USERLEN		10
#define	REALLEN	 	50
#define	TOPICLEN	307
/* DAL MADE ME PUT THIS IN THE FIEND:  --Russell
 * This number will be expanded to 200 in the near future
 */
#define	CHANNELLEN	32
#define	PASSWDLEN 	32	/* orig. 20, changed to 32 for nickpasswords */
#define	KEYLEN		23
#define	BUFSIZE		512		/* WARNING: *DONT* CHANGE THIS!!!! */
#define	MAXRECIPIENTS 	20
#define	MAXKILLS	20
#define	MAXBANS		60
#define	MAXBANLENGTH	1024
#define	MAXSILES	5
#define	MAXSILELENGTH	128
/*
 * Watch it - Don't change this unless you also change the ERR_TOOMANYWATCH
 * and PROTOCOL_SUPPORTED settings.
 */
#define MAXWATCH	128

#define	USERHOST_REPLYLEN	(NICKLEN+HOSTLEN+USERLEN+5)

#define READBUF_SIZE    16384   /* used in s_bsd *AND* s_zip.c ! */


#ifdef  ZIP_LINKS

/* the minimum amount of data needed to trigger compression */
#define ZIP_MINIMUM     4096

/* the maximum amount of data to be compressed (can actually be a bit more) */
#define ZIP_MAXIMUM     8192    /* WARNING: *DON'T* CHANGE THIS!!!! */

struct Zdata {
        z_stream        *in;            /* input zip stream data */
        z_stream        *out;           /* output zip stream data */
        char            inbuf[ZIP_MAXIMUM]; /* incoming zipped buffer */
        char            outbuf[ZIP_MAXIMUM]; /* outgoing (unzipped) buffer */
        int             incount;        /* size of inbuf content */
        int             outcount;       /* size of outbuf content */
};

#endif /* ZIP_LINKS */
/* NOTE: this must be down here so the stuff from struct.h IT uses works */
#include "whowas.h"


/*
** 'offsetof' is defined in ANSI-C. The following definition
** is not absolutely portable (I have been told), but so far
** it has worked on all machines I have needed it. The type
** should be size_t but...  --msa
*/
#ifndef offsetof
#define	offsetof(t,m) (int)((&((t *)0L)->m))
#endif

#define	elementsof(x) (sizeof(x)/sizeof(x[0]))

/*
** flags for bootup options (command line flags)
*/
#define	BOOT_CONSOLE	1
#define	BOOT_QUICK	2
#define	BOOT_DEBUG	4
#define	BOOT_INETD	8
#define	BOOT_TTY	16
#define	BOOT_OPER	32
#define	BOOT_AUTODIE	64

#define STAT_PING       -7	/* for UPING -GZ */
#define	STAT_LOG	-6	/* logfile for -x */
#define	STAT_MASTER	-5	/* Local ircd master before identification */
#define	STAT_CONNECTING	-4
#define	STAT_HANDSHAKE	-3
#define	STAT_ME		-2
#define	STAT_UNKNOWN	-1
#define	STAT_SERVER	0
#define	STAT_CLIENT	1
#define	STAT_SERVICE	2	/* Services not implemented yet */

/*
 * status macros.
 */
#define	IsRegisteredUser(x)	((x)->status == STAT_CLIENT)
#define	IsRegistered(x)		((x)->status >= STAT_SERVER)
#define	IsConnecting(x)		((x)->status == STAT_CONNECTING)
#define	IsHandshake(x)		((x)->status == STAT_HANDSHAKE)
#define	IsMe(x)			((x)->status == STAT_ME)
#define	IsUnknown(x)		((x)->status == STAT_UNKNOWN || \
				 (x)->status == STAT_MASTER)
#define	IsServer(x)		((x)->status == STAT_SERVER)
#define	IsClient(x)		((x)->status == STAT_CLIENT)
#define	IsLog(x)		((x)->status == STAT_LOG)
#define IsService(x)		0

#define	SetMaster(x)		((x)->status = STAT_MASTER)
#define	SetConnecting(x)	((x)->status = STAT_CONNECTING)
#define	SetHandshake(x)		((x)->status = STAT_HANDSHAKE)
#define	SetMe(x)		((x)->status = STAT_ME)
#define	SetUnknown(x)		((x)->status = STAT_UNKNOWN)
#define	SetServer(x)		((x)->status = STAT_SERVER)
#define	SetClient(x)		((x)->status = STAT_CLIENT)
#define	SetLog(x)		((x)->status = STAT_LOG)
#define	SetService(x)		((x)->status = STAT_SERVICE)

#define	FLAGS_PINGSENT   0x0001		/* Unreplied ping sent */
#define	FLAGS_DEADSOCKET 0x0002		/* Local socket is dead--Exiting soon */
#define	FLAGS_KILLED     0x0004		/* Prevents "QUIT" from being sent for this */
#define	FLAGS_BLOCKED    0x0008		/* socket is in a blocked condition */
#define	FLAGS_UNIX	 0x0010		/* socket is in the unix domain, not inet */
#define	FLAGS_CLOSING    0x0020		/* set when closing to suppress errors */
#define	FLAGS_LISTEN     0x0040 	/* used to mark clients which we listen() on */
#define	FLAGS_CHKACCESS  0x0080 	/* ok to check clients access if set */
#define	FLAGS_DOINGDNS	 0x0100 	/* client is waiting for a DNS response */
#define	FLAGS_AUTH	 0x0200 	/* client is waiting on rfc931 response */
#define	FLAGS_WRAUTH	 0x0400		/* set if we havent writen to ident server */
#define	FLAGS_LOCAL	 0x0800 	/* set for local clients */
#define	FLAGS_GOTID	 0x1000		/* successful ident lookup achieved */
#define	FLAGS_DOID	 0x2000		/* I-lines say must use ident return */
#define	FLAGS_NONL	 0x4000 	/* No \n in buffer */
#define FLAGS_TS8	 0x8000 	/* Why do you want to know? */
#define FLAGS_ULINE	0x10000 	/* User/server is considered U-lined */
#define FLAGS_SQUIT	0x20000 	/* Server has been /squit by an oper */
#define FLAGS_PROTOCTL	0x40000		/* Received a PROTOCTL message */
#define FLAGS_PING    	0x80000		/* UPING SENT */
#define FLAGS_ASKEDPING 0x100000	/* UPONG GOT*/

/* These 3 are related to the SOCKSPORT patch -Studded */
#define FLAGS_SOCKS     0x200000 	/* client is waiting on a rfc1928 response */
#define FLAGS_WRSOCKS  0x400000 	/* set if we havent written to socks server */
#define	FLAGS_GOTSOCKS 0x800000 	/* client has an open socks server */

#define FLAGS_MAP     	0x2000000 	/* Show this entry in /map */
#define FLAGS_JOINING 	0x4000000 	/* Prevent user from getting killed during JOIN */
#define FLAGS_ZIP     	0x8000000 	/* link is zipped */
#define FLAGS_ZIPFIRST  0x10000000 	/* start of zip (ignore any CRLF) */
#define FLAGS_NEWHOST	0x20000000	/* user is using newhost */
#define FLAGS_CBURST	0x40000000	/* set to mark connection burst being sent */
/* Need more flags - here's flags2 */

#define FLAGS2_HTC	0x00000001 /* Protect a user from getting killed with dead socket */


/* PROTOCOL FLAGS */
#define PTL_PTLINK  	0x00000001 	/* For PTlink synching */
#define PTL_UMODE	0x00000002	/* Use umode raw for mode changes */
#ifdef ENABLE_PTLINK		   
#	define VERSION_SEND (PTL_PTLINK|PTL_UMODE)
#else
#	define VERSION_SEND 0
#endif

/* Dec 26th, 1997 - having a go at
 * splitting flags into flags and umodes
 * -DuffJ
 */

#define	UMODE_INVISIBLE  0x0001 /* makes user invisible */
#define	UMODE_OPER       0x0002	/* Operator */
#define UMODE_WALLOP	 0x0004 /* Wallop */
#define UMODE_FAILOP     0x0008 /* Shows some global messages */
#define UMODE_BOT	 0x0010 /* User is a BOT/screen */
#define UMODE_REGNICK	 0x0020 /* Nick set by services as registered */
#define UMODE_SADMIN	 0x0040 /* Services Admin */
#define UMODE_ADMIN	 0x0080 /* Admin */
#define	UMODE_SERVNOTICE 0x0100 /* server notices such as kill */
#define	UMODE_LOCOP      0x0200 /* Local operator -- SRB */
#define UMODE_KILLS	 0x0400 /* Show server-kills... */
#define UMODE_CLIENT	 0x0800 /* Show client information */
#define UMODE_FLOOD	 0x1000 /* Receive flood warnings */
#define UMODE_ZOMBIE	 0x2000 /* Cannot send msgs */
#define UMODE_HELPOP	 0x4000 /* Can receive helpops */

/* Related to SEEUSERSTATS -Studded */
#define UMODE_STATS	 0x8000 /* Receive notice when users do /stats, et al */
#define UMODE_HIDE	0x10000 /* Admin,Oper usermask-hiding */
#define UMODE_WHOIS     0x20000 /* See USERHOST,WHOIS */
#define UMODE_NETADMIN	0x40000 /* Network Admin */
#define UMODE_TADMIN	0x80000	/* Technical Admin */
#define UMODE_VIRUS	0x100000	/* Virus infected user (DCC send blocked)*/
#define UMODE_PRIVACY	0x200000 	/* Hide channels from whois */

#define	SEND_UMODES (UMODE_INVISIBLE|UMODE_OPER|UMODE_FAILOP|UMODE_REGNICK|UMODE_SADMIN|UMODE_ADMIN|UMODE_HIDE|\
	UMODE_WHOIS|UMODE_NETADMIN|UMODE_TADMIN|UMODE_HELPOP|UMODE_ZOMBIE|UMODE_VIRUS|UMODE_PRIVACY)
#define	ALL_UMODES	(SEND_UMODES|UMODE_SERVNOTICE|UMODE_LOCOP|UMODE_KILLS|UMODE_CLIENT|UMODE_FLOOD|UMODE_STATS)
#define USER_MODES 	(UMODE_REGNICK|UMODE_HIDE|UMODE_WALLOP|UMODE_BOT|UMODE_HELPOP|UMODE_INVISIBLE|UMODE_VIRUS|\
	UMODE_PRIVACY)
#define	FLAGS_ID	(FLAGS_DOID|FLAGS_GOTID)

#define PROTO_NOQUIT	0x1	/* Negotiated NOQUIT protocol */
#define PROTO_TOKEN	0x2	/* Negotiated TOKEN protocol */

/*
 * flags macros.
 */
#define IsStatsF(x)		((x)->umodes & UMODE_STATS)
#define IsKillsF(x)		((x)->umodes & UMODE_KILLS)
#define IsClientF(x)		((x)->umodes & UMODE_CLIENT)
#define IsFloodF(x)		((x)->umodes & UMODE_FLOOD)
#define IsAdmin(x)		((x)->umodes & UMODE_ADMIN)
#define IsSAdmin(x)		((x)->umodes & UMODE_SADMIN)
#define IsTAdmin(x)		((x)->umodes & UMODE_TADMIN)
#define IsNetAdmin(x)		((x)->umodes & UMODE_NETADMIN)

#define SendFailops(x)		((x)->umodes & UMODE_FAILOP)
#define IsBot(x)		((x)->umodes & UMODE_BOT)
#define IsZombie(x)		((x)->umodes & UMODE_ZOMBIE)
#define IsHelpOp(x)		((x)->umodes & UMODE_HELPOP)
#define IsHidden(x)		((x)->umodes & UMODE_HIDE)
#define IsWmode(x)              ((x)->umodes & UMODE_WHOIS)
#define	IsOper(x)		((x)->umodes & UMODE_OPER)
#define	IsLocOp(x)		((x)->umodes & UMODE_LOCOP)
#define	IsInvisible(x)		((x)->umodes & UMODE_INVISIBLE)
#define	IsAnOper(x)		((x)->umodes & (UMODE_OPER|UMODE_LOCOP))
#define IsARegNick(x)		((x)->umodes & (UMODE_REGNICK))
#define IsRegNick(x)		((x)->umodes & UMODE_REGNICK)
#define IsInfected(x)		((x)->umodes & UMODE_VIRUS)
#define IsPrivate(x)		((x)->umodes & UMODE_PRIVACY)
#define	IsPerson(x)		((x)->user && IsClient(x))
#define	IsPrivileged(x)		(IsAnOper(x) || IsServer(x))
#define	SendServNotice(x)	((x)->umodes & UMODE_SERVNOTICE)
#define	IsUnixSocket(x)		((x)->flags & FLAGS_UNIX)
#define	IsListening(x)		((x)->flags & FLAGS_LISTEN)
#define	DoAccess(x)		((x)->flags & FLAGS_CHKACCESS)
#define	IsLocal(x)		((x)->flags & FLAGS_LOCAL)
#define	IsDead(x)		((x)->flags & FLAGS_DEADSOCKET)
#define CBurst(x)               ((x)->flags & FLAGS_CBURST)
#define GotProtoctl(x)		((x)->flags & FLAGS_PROTOCTL)
#define IsBlocked(x)		((x)->flags & FLAGS_BLOCKED)
#ifdef ENABLE_PTLINK
#	define IsVerPTlink(x)          ((x)->protocol & PTL_PTLINK)
#	define IsUmode2(x)	       ((x)->protocol & PTL_UMODE)
#else
#	define IsVerPTlink(x)           0    
#	define IsUmode2(x)              0
#endif

#define GotCapab(x)             ((x)->flags & FLAGS_GOTCAPAB)
#define IsNewHost(x)		((x)->flags & FLAGS_NEWHOST)

#ifdef NOSPOOF
#define	IsNotSpoof(x)		((x)->nospoof == 0)
#else
#define IsNotSpoof(x)           (1)
#endif

#ifdef SEEUSERSTATS
#define SetStatsF(x)		((x)->umodes |= UMODE_STATS)
#endif
#define SetKillsF(x)		((x)->umodes |= UMODE_KILLS)
#define SetClientF(x)		((x)->umodes |= UMODE_CLIENT)
#define SetFloodF(x)		((x)->umodes |= UMODE_FLOOD)
#define SetHidden(x)		((x)->umodes |= UMODE_HIDE)
#define	SetOper(x)		((x)->umodes |= UMODE_OPER)
#define	SetLocOp(x)    		((x)->umodes |= UMODE_LOCOP)
#define	SetInvisible(x)		((x)->umodes |= UMODE_INVISIBLE)
#define SetWmode(x)             ((x)->umodes |= UMODE_WHOIS)
#define	SetUnixSock(x)		((x)->flags |= FLAGS_UNIX)
#define	SetDNS(x)		((x)->flags |= FLAGS_DOINGDNS)
#define	DoingDNS(x)		((x)->flags & FLAGS_DOINGDNS)
#define	SetAccess(x)		((x)->flags |= FLAGS_CHKACCESS)
#define SetBlocked(x)		((x)->flags |= FLAGS_BLOCKED)
#define SetVerPTlink(x)         ((x)->flags |= FLAGS_PTLINK)
#define SetCapab(x)             ((x)->flags |= FLAGS_GOTCAPAB)
#define SetNewHost(x)		((x)->flags |= FLAGS_NEWHOST)
#define	DoingAuth(x)		((x)->flags & FLAGS_AUTH)

#ifdef SOCKSPORT
#define	DoingSocks(x)		((x)->flags & FLAGS_SOCKS)
#endif

#define	NoNewLine(x)		((x)->flags & FLAGS_NONL)
#define SetRegNick(x)		((x)->umodes & UMODE_REGNICK)

#ifdef SEEUSERSTATS
#define ClearStatsF(x)		((x)->umodes &= ~UMODE_STATS)
#endif
#define ClearAdmin(x)		((x)->umodes &= ~UMODE_ADMIN)
#define ClearSAdmin(x)		((x)->umodes &= ~UMODE_SADMIN)
#define ClearTechAdmin(x)		((x)->umodes &= ~UMODE_TADMIN)
#define ClearNetAdmin(x)	((x)->umodes &= ~UMODE_NETADMIN)
#define ClearHelpOp(x)		((x)->umodes &= ~UMODE_HELPOP)
#define ClearKillsF(x)		((x)->umodes &= ~UMODE_KILLS)
#define ClearClientF(x)		((x)->umodes &= ~UMODE_CLIENT)
#define ClearFloodF(x)		((x)->umodes &= ~UMODE_FLOOD)
#define ClearFailops(x)		((x)->umodes &= ~UMODE_FAILOP)
#define ClearHidden(x)          ((x)->umodes &= ~UMODE_HIDE)
#define ClearWmode(x)           ((x)->umodes &= ~UMODE_WHOIS)
#define	ClearOper(x)		((x)->umodes &= ~UMODE_OPER)
#define	ClearInvisible(x)	((x)->umodes &= ~UMODE_INVISIBLE)
#define	ClearDNS(x)		((x)->flags &= ~FLAGS_DOINGDNS)
#define	ClearAuth(x)		((x)->flags &= ~FLAGS_AUTH)
#define ClearVerPTlink(x)       ((x)->flags &= ~FLAGS_PTLINK)
#define ClearCapab(x)           ((x)->flags &= ~FLAGS_GOTCAPAB)
#define ClearNewHost(x)           ((x)->flags &= ~FLAGS_NEWHOST)
#ifdef SOCKSPORT
#define	ClearSocks(x)		((x)->flags &= ~FLAGS_SOCKS)
#endif

#define	ClearAccess(x)		((x)->flags &= ~FLAGS_CHKACCESS)
#define ClearBlocked(x)		((x)->flags &= ~FLAGS_BLOCKED)

/*
 * ProtoCtl options
 */
#define DontSendQuit(x)		((x)->proto & PROTO_NOQUIT)

/* For compatibilty with PTlink ircd 3.x disable token */
#define IsToken(x)		(((x)->proto & PROTO_TOKEN) && IsVerPTlink(x))

#define SetNoQuit(x)		((x)->proto |= PROTO_NOQUIT)
#define SetToken(x)		((x)->proto |= PROTO_TOKEN)

#define ClearNoQuit(x)		((x)->proto &= ~PROTO_NOQUIT)
#define ClearToken(x)		((x)->proto &= ~PROTO_TOKEN)


#define MaskHost(x)		((x)->user->mask)
/*
 * defined operator access levels
 */
#define OFLAG_REHASH	0x00000001  /* Oper can /rehash server */
#define OFLAG_DIE	0x00000002  /* Oper can /die the server */
#define OFLAG_RESTART	0x00000004  /* Oper can /restart the server */

/* 0x00000010 available */

#define OFLAG_GLOBOP	0x00000020  /* Oper can send /GlobOps */
#define OFLAG_LOCOP	0x00000080  /* Oper can send /LocOps */
#define OFLAG_LROUTE	0x00000100  /* Oper can do local routing */
#define OFLAG_GROUTE	0x00000200  /* Oper can do global routing */
#define OFLAG_LKILL	0x00000400  /* Oper can do local kills */
#define OFLAG_GKILL	0x00000800  /* Oper can do global kills */
#define OFLAG_KLINE	0x00001000  /* Oper can /kline users */
#define OFLAG_UNKLINE	0x00002000  /* Oper can /unkline users */
#define OFLAG_LNOTICE	0x00004000  /* Oper can send local serv notices */
#define OFLAG_GNOTICE	0x00008000  /* Oper can send global notices */
#define OFLAG_ADMIN	0x00010000  /* Admin */
#define OFLAG_UMODEC	0x00020000  /* Oper can set umode +c */
#define OFLAG_UMODEF	0x00040000  /* Oper can set umode +f */
#define OFLAG_NETADMIN  0x00080000  /* Oper can be a network admin */
#define OFLAG_ZLINE	0x00100000  /* Oper can use /zline and /unzline */
#define OFLAG_TADMIN	0x00200000  /* Technical administrator */

#define OFLAG_LOCAL	(OFLAG_REHASH|OFLAG_GLOBOP|OFLAG_LOCOP|OFLAG_LROUTE|OFLAG_LKILL|OFLAG_KLINE|OFLAG_UNKLINE|OFLAG_LNOTICE|OFLAG_UMODEC|OFLAG_UMODEF)
#define OFLAG_GLOBAL	(OFLAG_LOCAL|OFLAG_GROUTE|OFLAG_GKILL|OFLAG_GNOTICE)
#define OFLAG_ISGLOBAL	(OFLAG_GROUTE|OFLAG_GKILL|OFLAG_GNOTICE)

#define OPCanRehash(x)	((x)->oflag & OFLAG_REHASH)
#define OPCanDie(x)	((x)->oflag & OFLAG_DIE)
#define OPCanRestart(x)	((x)->oflag & OFLAG_RESTART)
#define OPCanGlobOps(x)	((x)->oflag & OFLAG_GLOBOP)
#define OPCanLocOps(x)	((x)->oflag & OFLAG_LOCOP)
#define OPCanLRoute(x)	((x)->oflag & OFLAG_LROUTE)
#define OPCanGRoute(x)	((x)->oflag & OFLAG_GROUTE)
#define OPCanLKill(x)	((x)->oflag & OFLAG_LKILL)
#define OPCanGKill(x)	((x)->oflag & OFLAG_GKILL)
#define OPCanKline(x)	((x)->oflag & OFLAG_KLINE)
#define OPCanUnKline(x)	((x)->oflag & OFLAG_UNKLINE)
#define OPCanLNotice(x)	((x)->oflag & OFLAG_LNOTICE)
#define OPCanGNotice(x)	((x)->oflag & OFLAG_GNOTICE)
#define OPIsAdmin(x)	((x)->oflag & OFLAG_ADMIN)
#define OPIsTechAdmin(x)((x)->oflag & OFLAG_TADMIN)
#define OPIsNetAdmin(x)	((x)->oflag & OFLAG_NETADMIN)
#define OPCanUModeC(x)	((x)->oflag & OFLAG_UMODEC)
#define OPCanUModeF(x)	((x)->oflag & OFLAG_UMODEF)

#define OPSetRehash(x)	((x)->oflag |= OFLAG_REHASH)
#define OPSetDie(x)	((x)->oflag |= OFLAG_DIE)
#define OPSetRestart(x)	((x)->oflag |= OFLAG_RESTART)
#define OPSetGlobOps(x)	((x)->oflag |= OFLAG_GLOBOP)
#define OPSetLocOps(x)	((x)->oflag |= OFLAG_LOCOP)
#define OPSetLRoute(x)	((x)->oflag |= OFLAG_LROUTE)
#define OPSetGRoute(x)	((x)->oflag |= OFLAG_GROUTE)
#define OPSetLKill(x)	((x)->oflag |= OFLAG_LKILL)
#define OPSetGKill(x)	((x)->oflag |= OFLAG_GKILL)
#define OPSetKline(x)	((x)->oflag |= OFLAG_KLINE)
#define OPSetUnKline(x)	((x)->oflag |= OFLAG_UNKLINE)
#define OPSetLNotice(x)	((x)->oflag |= OFLAG_LNOTICE)
#define OPSetGNotice(x)	((x)->oflag |= OFLAG_GNOTICE)
#define OPSSetAdmin(x)	((x)->oflag |= OFLAG_ADMIN)
#define OPSSetSAdmin(x) ((x)->oflag |= OFLAG_SADMIN)
#define OPSetUModeC(x)	((x)->oflag |= OFLAG_UMODEC)
#define OPSetUModeF(x)	((x)->oflag |= OFLAG_UMODEF)
#define OPSetZLine(x)	((x)->oflag |= OFLAG_ZLINE)

#define OPClearRehash(x)	((x)->oflag &= ~OFLAG_REHASH)
#define OPClearDie(x)		((x)->oflag &= ~OFLAG_DIE)  
#define OPClearRestart(x)	((x)->oflag &= ~OFLAG_RESTART)
#define OPClearGlobOps(x)	((x)->oflag &= ~OFLAG_GLOBOP)
#define OPClearLocOps(x)	((x)->oflag &= ~OFLAG_LOCOP)
#define OPClearLRoute(x)	((x)->oflag &= ~OFLAG_LROUTE)
#define OPClearGRoute(x)	((x)->oflag &= ~OFLAG_GROUTE)
#define OPClearLKill(x)		((x)->oflag &= ~OFLAG_LKILL)
#define OPClearGKill(x)		((x)->oflag &= ~OFLAG_GKILL)
#define OPClearKline(x)		((x)->oflag &= ~OFLAG_KLINE)
#define OPClearUnKline(x)	((x)->oflag &= ~OFLAG_UNKLINE)
#define OPClearLNotice(x)	((x)->oflag &= ~OFLAG_LNOTICE)
#define OPClearGNotice(x)	((x)->oflag &= ~OFLAG_GNOTICE)
#define OPClearAdmin(x)		((x)->oflag &= ~OFLAG_ADMIN)
#define OPClearSAdmin(x)	((x)->oflag &= ~OFLAG_SADMIN)
#define OPClearUModeC(x)	((x)->oflag &= ~OFLAG_UMODEC)
#define OPClearUModeF(x)	((x)->oflag &= ~OFLAG_UMODEF)
#define OPClearZLine(x)		((x)->oflag &= ~OFLAG_ZLINE)

/*
 * defined debugging levels
 */
#define	DEBUG_FATAL  0
#define	DEBUG_ERROR  1	/* report_error() and other errors that are found */
#define	DEBUG_NOTICE 3
#define	DEBUG_DNS    4	/* used by all DNS related routines - a *lot* */
#define	DEBUG_INFO   5	/* general usful info */
#define	DEBUG_NUM    6	/* numerics */
#define	DEBUG_SEND   7	/* everything that is sent out */
#define	DEBUG_DEBUG  8	/* anything to do with debugging, ie unimportant :) */
#define	DEBUG_MALLOC 9	/* malloc/free calls */
#define	DEBUG_LIST  10	/* debug list use */

/*
 * UPING Start
 */

#define ClearPing(x)            ((x)->flags &= ~FLAGS_PING)
#define ClearAskedPing(x)       ((x)->flags &= ~FLAGS_ASKEDPING)
#define SetAskedPing(x)         ((x)->flags |= FLAGS_ASKEDPING)
#define AskedPing(x)            ((x)->flags & FLAGS_ASKEDPING)
#define DoPing(x)               ((x)->flags & FLAGS_PING)
#define IsPing(x)               ((x)->status == STAT_PING)
#define SetPing(x)              ((x)->status = STAT_PING)

/*
 * defines for curses in client
 */
#define	DUMMY_TERM	0
#define	CURSES_TERM	1
#define	TERMCAP_TERM	2

struct  SqlineItem	{
	unsigned int	status;
	char *sqline;
	char *reason;
	struct	SqlineItem *next;
};

struct	ConfItem	{
	unsigned int	status;	/* If CONF_ILLEGAL, delete when no clients */
	int	clients;	/* Number of *LOCAL* clients using this */
	struct	in_addr ipnum;	/* ip number of host field */
	char	*host;
	char	*passwd;
	char	*name;
	int	port;
	time_t	hold;	/* Hold action until this time (calendar time) */
	int	tmpconf;
#ifndef VMSP
	aClass	*class;  /* Class of connection */
#endif
	struct	ConfItem *next;
};

#define	CONF_ILLEGAL		0x80000000
#define	CONF_MATCH		0x40000000
#define	CONF_QUARANTINED_SERVER	0x0001
#define	CONF_CLIENT		0x0002
#define	CONF_CONNECT_SERVER	0x0004
#define	CONF_NOCONNECT_SERVER	0x0008
#define	CONF_LOCOP		0x0010
#define	CONF_OPERATOR		0x0020
#define	CONF_ME			0x0040
#define	CONF_KILL		0x0080
#define	CONF_ADMIN		0x0100
#ifdef 	R_LINES
#define	CONF_RESTRICT		0x0200
#endif
#define	CONF_CLASS		0x0400
#define	CONF_SERVICE		0x0800
#define	CONF_LEAF		0x1000
#define	CONF_LISTEN_PORT	0x2000
#define	CONF_HUB		0x4000
#define	CONF_UWORLD		0x8000
#define CONF_QUARANTINED_NICK	0x10000
#define CONF_ZAP		0x20000
#define CONF_CONFIG             0x100000
#define CONF_CRULEALL           0x200000
#define CONF_CRULEAUTO          0x400000
#define CONF_MISSING		0x800000
#define CONF_SADMIN		0x1000000
#define CONF_DRPASS		0x2000000   /* DIE/RESTART pass - NikB */
#define CONF_ZTIME		0x4000000   /* zline timer -taz */
#define CONF_SOCKSEXCEPT	0x8000000   /* socks detect exception */

#define	CONF_OPS		(CONF_OPERATOR | CONF_LOCOP)
#define	CONF_SERVER_MASK	(CONF_CONNECT_SERVER | CONF_NOCONNECT_SERVER)
#define	CONF_CLIENT_MASK	(CONF_CLIENT | CONF_SERVICE | CONF_OPS | \
				 CONF_SERVER_MASK)
#define CONF_CRULE              (CONF_CRULEALL | CONF_CRULEAUTO)
#define CONF_QUARANTINE		(CONF_QUARANTINED_SERVER|CONF_QUARANTINED_NICK)

#define	IsIllegal(x)	((x)->status & CONF_ILLEGAL)
#define IsTemp(x)	((x)->tmpconf)


/*
 * Client structures
 */
struct	User	{
	struct	User	*nextu;
	Link	*channel;	/* chain of channel pointer blocks */
	Link	*invited;	/* chain of invite pointer blocks */
	Link	*silence;	/* chain of silence pointer blocks */
	char	*away;		/* pointer to away message */
	time_t	last;
	u_int32_t servicestamp;  /* Services' time stamp variable */
	u_int32_t news_mask;    /* Services user news bit mask */
	int	refcnt;		/* Number of times this block is referenced */
	int	joined;		/* number of channels joined */
	char	username[USERLEN+1];
	char	host[HOSTLEN+1];
	char	mask[HOSTLEN+1];
        char	server[HOSTLEN+1];
//        char    hidden[5];      /* Used for /whowas masking -GZ */
				/*
				** In a perfect world the 'server' name
				** should not be needed, a pointer to the
				** client describing the server is enough.
				** Unfortunately, in reality, server may
				** not yet be in links while USER is
				** introduced... --msa
				** No.  We are going to assume the server
				** is there.  There is no good reason at
				** all that we should get a NICK line
				** before a SERVER line.   -- Aeto
				** BUT, it is hiddenin aClient! -- Aeto
				*/
#ifdef	LIST_DEBUG
	aClient	*bcptr;
#endif
};

struct	Server	{
	struct	Server	*nexts;
	anUser	*user;		/* who activated this connection */
	char	up[HOSTLEN+1];	/* uplink for this server */
	char	by[NICKLEN+1];
	aConfItem *nline;	/* N-line pointer for this server */
#ifdef	LIST_DEBUG
	aClient	*bcptr;
#endif
};

/* SJOIN synch structure */
struct SynchList {
	char		nick[NICKLEN];
	int		deop;
	int		devoice;
	int		op;
	int		voice;
	aSynchList	*next, *prev;
};

/* Mode buffer for SJOIN parsing - Lamego */
typedef struct ModeBuffer aModeBuffer;
struct ModeBuffer {
    aClient *user;
    unsigned int modes;
};

/* GZ-line structure */
struct GZline {
        char 		ip[20];
        char            ident[20];
	char		reason[80];
        char 		from[NICKLEN];
	int		flag;
	int		duration;
	int		timeset;
	aGZline		*next, *prev;
};

/* For gline time settings */
#define TStime() time(NULL)
/* G-line structure */
struct t_gline {
        char *usermask;
        char *hostmask;
        char *reason;
        char *setby;
        time_t expire_at;
        time_t set_at;
        aGline *next;
        aGline *prev;
};

/* V-line structure */
struct t_vline {
	char *mask;
        char *reason;
        aVline *next;
        aVline *prev;
};


/* Related to zline timer for SOCKS -taz */
struct Event {
	void		(*func)(char *);
	char		arg[ARGLEN+1];
       	u_int32_t	exectime;
        aEvent		*next, *prev;
};

struct Client	{
	struct	Client *next, *prev, *hnext;
	anUser	*user;		/* ...defined, if this is a User */
	aServer	*serv;		/* ...defined, if this is a server */
/*	int     tag;*/
	int	hashv;		/* raw hash value (according to ircu) -- was
				 * added so that UPING would work. Dreamforge
				 * 4.6.7b Not needed in b ? I dunno they said so.
				 */
	time_t	lasttime;	/* ...should be only LOCAL clients? --msa */
	time_t	firsttime;	/* time client was created */
	time_t	since;		/* last time we parsed something */
	time_t	lastnick;	/* TimeStamp on nick */
	time_t  nextnick;	/* Time the next nick change will be allowed */
	time_t  nexttarget;	/* Time until a change in targets is allowed */
	time_t  lasthtc;	/* Time the user used a high traffic command for the last time */
	int	htccount;	/* Counter for HTC commands */
	int	htcignore;	/* flag for HTC ignore on/off */
	u_char targets[MAXTARGETS]; /* Hash values of current targets */	
	long	flags;		/* client flags */
	long	flags2;		/* more client flags */
	long	protocol;	/* connection protocol */
	long	umodes;		/* client usermodes */
	aClient	*from;		/* == self, if Local Client, *NEVER* NULL! */
	int	fd;		/* >= 0, for local clients */
	int	hopcount;	/* number of servers to this 0 = local */
	short	status;		/* Client type */
	char	name[HOSTLEN+1]; /* Unique name of the client, nick or host */
	char	username[USERLEN+1]; /* username here now for auth stuff */
	char	info[REALLEN+1]; /* Free form additional client information */
	char	version[REALLEN+1]; /* version of the client (servers only) -GZ */
//        char    hidden[5];       /* Used for /whowas masking -GZ */
	aClient	*srvptr;	/* Server introducing this.  May be &me */
	Link	*history;	/* Whowas linked list */
	/*
	** The following fields are allocated only for local clients
	** (directly connected to *this* server with a socket.
	** The first of them *MUST* be the "count"--it is the field
	** to which the allocation is tied to! *Never* refer to
	** these fields, if (from != self).
	*/
	int	count;		/* Amount of data in buffer */
	char	buffer[BUFSIZE]; /* Incoming message buffer */
#ifdef  ZIP_LINKS
        aZdata  *zip;		/* zip data */
#endif	
	short	lastsq;		/* # of 2k blocks when sendqueued called last*/
	dbuf	sendQ;		/* Outgoing message queue--if socket full */
	dbuf	recvQ;		/* Hold for data incoming yet to be parsed */
#ifdef NOSPOOF
	u_int32_t	nospoof;	/* Anti-spoofing random number */
#endif
	long	oflag;		/* Operator access flags -Cabal95 */
	long	proto;		/* ProtoCtl options */
	long	sendM;		/* Statistics: protocol messages send */
	long	sendK;		/* Statistics: total k-bytes send */
	long	receiveM;	/* Statistics: protocol messages received */
	long	receiveK;	/* Statistics: total k-bytes received */
	long	previousSQK;	/* Statistics: used for bandwith-meter      */
	long	previousRQK;	/* Statistics: used for bandwith-meter      */
	long	deltaSQK;	/* Statistics: used for bandwith-meter      */
	long	deltaRQK;	/* Statistics: used for bandwith-meter - GZ */
		u_short	sendB;		/* counters to count upto 1-k lots of bytes */
	long	connectQ;	/* The count of bytes sent during connect */
	u_short	receiveB;	/* sent and received. */
	aClient	*acpt;		/* listening client which we accepted from */
	Link	*confs;		/* Configuration record associated */
	int	authfd;		/* fd for rfc931 authentication */
#ifdef SOCKSPORT
	int	socksfd;	/* fd for rfc1928 checking */
#endif
	struct	in_addr	ip;	/* keep real ip# too */
	u_short	port;		/* and the remote port# too :-) */
	struct	hostent	*hostp;
	u_short	notifies;	/* Keep track of count of notifies */
	Link	*notify;	/* Links to clients notify-structures */
	LOpts	*lopt;		/* Saved /list options */
#ifdef	pyr
	struct	timeval	lw;
#endif
	char	sockhost[HOSTLEN+1]; /* This is the host name from the socket
				     ** and after which the connection was
				     ** accepted.
				     */
	char	passwd[PASSWDLEN+1];
	int     caps;           /* capabilities bit-field */	
#ifdef DEBUGMODE
	time_t	cputime;
#endif
};

#define	CLIENT_LOCAL_SIZE sizeof(aClient)
#define	CLIENT_REMOTE_SIZE offsetof(aClient,count)

/*
 * statistics structures
 */
struct	stats {
	unsigned int	is_cl;	/* number of client connections */
	unsigned int	is_sv;	/* number of server connections */
	unsigned int	is_ni;	/* connection but no idea who it was */
	unsigned short	is_cbs;	/* bytes sent to clients */
	unsigned short	is_cbr;	/* bytes received to clients */
	unsigned short	is_sbs;	/* bytes sent to servers */
	unsigned short	is_sbr;	/* bytes received to servers */
	unsigned long	is_cks;	/* k-bytes sent to clients */
	unsigned long	is_ckr;	/* k-bytes received to clients */
	unsigned long	is_sks;	/* k-bytes sent to servers */
	unsigned long	is_skr;	/* k-bytes received to servers */
	time_t 		is_cti;	/* time spent connected by clients */
	time_t		is_sti;	/* time spent connected by servers */
	unsigned int	is_ac;	/* connections accepted */
	unsigned int	is_ref;	/* accepts refused */
	unsigned int	is_unco; /* unknown commands */
	unsigned int	is_wrdi; /* command going in wrong direction */
	unsigned int	is_unpf; /* unknown prefix */
	unsigned int	is_empt; /* empty message */
	unsigned int	is_num;	/* numeric message */
	unsigned int	is_kill; /* number of kills generated on collisions */
	unsigned int	is_fake; /* MODE 'fakes' */
	unsigned int	is_asuc; /* successful auth requests */
	unsigned int	is_abad; /* bad auth requests */
	unsigned int	is_udp;	/* packets recv'd on udp port */
	unsigned int	is_loc;	/* local connections made */
};

struct ListOptions {
	LOpts	*next;
	Link	*yeslist, *nolist;
	int	starthash;
	short int	showall;
	unsigned short	usermin;
	int	usermax;
	time_t	currenttime;
	time_t	chantimemin;
	time_t	chantimemax;
	time_t	topictimemin;
	time_t	topictimemax;
};

/* mode structure for channels */

struct	SMode	{
	unsigned int	mode;
	int	limit;
	char	key[KEYLEN+1];
};

/* Message table structure */

struct	Message	{
	char	*cmd;
	int	(* func)();
	unsigned int	count;
	int	parameters;
	char	flags;
		/* bit 0 set means that this command is allowed to be used
		 * only on the average of once per 2 seconds -SRB */
	u_char	token[2]; /* Cheat for tokenized value */
	unsigned long	bytes;
#ifdef DEBUGMODE
	unsigned long	lticks;
	unsigned long	rticks;
#endif
};

/* Used for notify-hash buckets... -Donwulff */

struct Notify {
	aNotify	*hnext;
	time_t	lasttime;
	Link	*notify;
	char	nick[1];
};

/* general link structure used for chains */

struct	SLink	{
	struct	SLink	*next;
	int	flags;
	union {
		aClient	*cptr;
		aChannel *chptr;
		aConfItem *aconf;
		aNotify	*nptr;
		aName *whowas;
		char	*cp;
	} value;
};

struct	SBan	{
	struct	SBan	*next;
	char *banstr;
	char *who;
	time_t when;
};

/* channel structure */

struct Channel	{
	struct	Channel *nextch, *prevch, *hnextch;
	Mode	mode;
	time_t	creationtime;
	char	topic[TOPICLEN+1];
	char	topic_nick[NICKLEN+1];
	time_t	topic_time;
	int	users;
	Link	*members;
	Link	*invites;
	Ban	*banlist;
	Ban	*exbanlist;
	char	chname[1];
};

/*
** Channel Related macros follow
*/

/* Channel related flags */

#define	CHFL_CHANOP     0x0001 /* Channel operator */
#define	CHFL_VOICE      0x0002 /* the power to speak */
#define	CHFL_DEOPPED	0x0004 /* Is de-opped by a server */
#define	CHFL_SERVOPOK   0x0008 /* Server op allowed */
/* zombies removed
#define	CHFL_ZOMBIE     0x0010 (* Kicked from channel *)
*/
/* Bans are stored in separate linked list, so phase this out? */
#define	CHFL_BAN	0x0020 /* ban channel flag */
#define CHFL_ADMIN	0x0040 /* protected user */
#define	CHFL_OVERLAP    (CHFL_CHANOP|CHFL_VOICE|CHFL_ADMIN)

/* Channel Visibility macros */

#define	MODE_CHANOP	CHFL_CHANOP
#define	MODE_VOICE	CHFL_VOICE
#define MODE_ADMIN	CHFL_ADMIN
#define	MODE_PRIVATE	0x0004
#define	MODE_SECRET	0x0008
#define	MODE_MODERATED  0x0010
#define	MODE_TOPICLIMIT 0x0020
#define	MODE_INVITEONLY 0x0040
#define	MODE_NOPRIVMSGS 0x0080
#define	MODE_KEY	0x0100
#define	MODE_BAN	0x0200
#define	MODE_LIMIT	0x0400
#define MODE_EXBAN      0x0600
#define MODE_RGSTR	0x0800
#define MODE_RGSTRONLY  0x1000
#define MODE_NOCOLORS   0x2000		/* Colour-removing mode -Defiant */
#define MODE_KNOCK	0x4000		/* shows users trying to join +i/+k channels -Lamego */
#define MODE_OPERONLY	0x8000  	/* IRC operators only */
#define	MODE_ADMINONLY	0x10000  	/* IRC admins only */
#define MODE_NOSPAM	0x20000		/* no spam messages allowed from non @/+ */

/*
 * mode flags which take another parameter (With PARAmeterS)
 */
#define	MODE_WPARAS	(MODE_CHANOP|MODE_VOICE|MODE_BAN|MODE_ADMIN|MODE_EXBAN|MODE_KEY|MODE_LIMIT)
/*
 * Undefined here, these are used in conjunction with the above modes in
 * the source.
#define	MODE_DEL       0x40000000
#define	MODE_ADD       0x80000000
 */

#define	HoldChannel(x)		(!(x))
/* name invisible */
#define	SecretChannel(x)	((x) && ((x)->mode.mode & MODE_SECRET))
/* channel not shown but names are */
#define	HiddenChannel(x)	((x) && ((x)->mode.mode & MODE_PRIVATE))
/* channel visible */
#define	ShowChannel(v,c)	(PubChannel(c) || IsSAdmin(v) || IsMember((v),(c)))
#define	PubChannel(x)		((!x) || ((x)->mode.mode &\
				 (MODE_PRIVATE | MODE_SECRET)) == 0)

/* #define	IsChannelName(name) ((name) && ((*(name) == '#') || (*(name) == '&') || ((*(name) == '+') && (*(name + 1) != '#')))) */
#define	IsChannelName(name) ((name) && ((*(name) == '#') || (*(name) == '&')))
#define IsModelessChannel(name) ((name) && (*(name) == '+'))

/* misc defines */
#define GO_ON           -3      /* for m_nick/m_client's benefit */

/* Misc macros */

#define	BadPtr(x) (!(x) || (*(x) == '\0'))

#define	isvalid(c) (((c) >= 'A' && (c) <= '~') || isdigit(c) || (c) == '-')

#define	MyConnect(x)			((x)->fd >= 0)
#define	MyClient(x)			(MyConnect(x) && IsClient(x))
#define	MyOper(x)			(MyConnect(x) && IsOper(x))

/* Lifted somewhat from Undernet code --Rak */

#define IsSendable(x)		(DBufLength(&x->sendQ) < 2048)
#define DoList(x)		((x)->lopt)

/* String manipulation macros */

/* strncopynt --> strncpyzt to avoid confusion, sematics changed
   N must be now the number of bytes in the array --msa */
#define	strncpyzt(x, y, N) do{(void)strncpy(x,y,N);x[N-1]='\0';}while(0)
#define	StrEq(x,y)	(!strcmp((x),(y)))

/* used in SetMode() in channel.c and m_umode() in s_msg.c */

#define	MODE_NULL      0
#define	MODE_ADD       0x40000000
#define	MODE_DEL       0x20000000

/* return values for hunt_server() */

#define	HUNTED_NOSUCH	(-1)	/* if the hunted server is not found */
#define	HUNTED_ISME	0	/* if this server should execute the command */
#define	HUNTED_PASS	1	/* if message passed onwards successfully */

/* used when sending to #mask or $mask */

#define	MATCH_SERVER  1
#define	MATCH_HOST    2

/* used for async dns values */

#define	ASYNC_NONE	(-1)
#define	ASYNC_CLIENT	0
#define	ASYNC_CONNECT	1
#define	ASYNC_CONF	2
#define	ASYNC_SERVER	3
#define ASYNC_PING	4	/* for UPING */

/* misc variable externs */

extern	char	*version, *infotext[];
extern	char	*generation, *creation;

/* misc defines */

#define	FLUSH_BUFFER	-2
#define	UTMP		"/etc/utmp"
#define	COMMA		","

/* IRC client structures */

#define     UDPPORT         "7007"	/* For UPING */

struct DSlink {
  struct DSlink *next;
  struct DSlink *prev;
  union {
    aClient *cptr;
    struct Channel *chptr;
    struct ConfItem *aconf;
    char *cp;
  } value;
};

#define CAP_TS          0x00000001
#define CAP_TS4         0x00000002
#define CAP_ZIP         0x00000004

#define DoesTS(x)       ((x)->caps & (CAP_TS|CAP_TS4))
/* we can't #define DoesTS to 1, it's used to check that the server sent
** the "TS" extra arg to "PASS" (or "CAPAB TS4" or both).
*/


typedef int (*testcap_t)(aClient *, int);
#define SEND_DEFAULT    (testcap_t)0

struct  Capability      {
        char    *name;
        int     cap;
        int     wehaveit;
        int     required;
        testcap_t  sendit;      /* SEND_DEFAULT means: send it if we have it */
};

/*
 * Capability macros.
 */
#define IsCapable(x, cap)       ((x)->caps & (cap))
#define SetCapable(x, cap)      ((x)->caps |= (cap))
#define ClearCap(x, cap)        ((x)->caps &= ~(cap))

//extern  int     test_send_zipcap PROTO((aClient *, int));

/*
** list of recognized server capabilities.  "TS" is not on the list
** because all servers that we talk to already do TS, and the kludged
** extra argument to "PASS" takes care of checking that.  -orabidoo
*/

#ifdef CAPTAB
struct Capability captab[] = {
/*  name        cap           have   required  send function */
#ifdef  ZIP_LINKS
  { "ZIP",      CAP_ZIP,        1,      0,      SEND_DEFAULT },
#else
  { "ZIP",      CAP_ZIP,        0,      0,      SEND_DEFAULT },
#endif
  { (char*)0,   0,              0,      0,      SEND_DEFAULT }
};
#else
extern  struct Capability captab[];
#endif

#endif /* __struct_include__ */
