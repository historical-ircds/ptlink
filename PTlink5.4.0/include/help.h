/****************************************************
 * help.h - /helpsys related routines               *
 ****************************************************
 * (C) PTlink Coders Team 2000 - Lamego@PTlink.net  *
 ****************************************************/
#ifndef HELP_H
#define HELP_H
typedef struct helpitem_ HelpItem;
struct helpitem_ {
    char* topic;
    char** text;
    HelpItem *next;
};
int help_load(char *fn, HelpItem **hi);
char** help_find(char* topic, HelpItem *hi);
void help_free(HelpItem **hi);
#endif