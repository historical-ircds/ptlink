/*
 *   IRC - Internet Relay Chat, include/sys.h
 *   Copyright (C) 1990 University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__sys_include__
#define __sys_include__
#ifdef ISC202
#include <net/errno.h>
#else
#include <sys/errno.h>
#endif

#include "setup.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/param.h>

#ifdef	HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef	HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef	HAVE_STRINGS_H
#include <strings.h>
#else
# ifdef	HAVE_STRING_H
# include <string.h>
# endif
#endif
#ifndef HAVE_STRCASECMP
#define	strcasecmp	mycmp
#endif
#ifndef HAVE_INDEX
#define   index   strchr
#define   rindex  strrchr
/*
extern	char	*index PROTO((char *, char));
extern	char	*rindex PROTO((char *, char));
*/
#endif

#ifndef HAVE_BZERO
#define bzero(a,b)      memset(a,0,b)
#define bcopy(a,b,c)    memcpy(b,a,c)
#define bcmp    memcmp
#endif

#ifdef HAVE_GETRUSAGE_2
#  define GETRUSAGE_2
#else
#  ifdef HAVE_TIMES_2
#    define TIMES_2
#   endif 		/* HAVE_TIMES_2 */
#endif 			/* GETRUSAGE_2 */

#ifdef HAVE_POSIX_NBLOCK
#  define NBLOCK_POSIX
#else           /* HAVE_POSIX_NBLOCK */
#  ifdef HAVE_BSD_NBLOCK 
#    define NBLOCK_BSD
#  else 
#    define NBLOCK_SYSV
#  endif 		/* HAVE_BSD_NBLOCK */
#endif 		/* HAVE_POSIX_NBLOCK */

#ifdef HAVE_POSIX_SIGNALS
#  define POSIX_SIGNALS
#else
#  ifdef HAVE_BSD_SIGNALS
#    define BSD_RELIABLE_SIGNALS
#  else
#    define SYSV_UNRELIABLE_SIGNALS
#  endif 		/* HAVE_BSD_SIGNALS */
#endif 		/* HAVE_POSIX_SIGNALS */

#ifdef AIX
#  include <sys/select.h>
#  include <sys/machine.h>
#  define BSD_INCLUDES
#  if BYTE_ORDER == BIG_ENDIAN
#  define BIT_ZERO_ON_LEFT
# endif
# if BYTE_ORDER == LITTLE_ENDIAN
#  define BIT_ZERO_ON_RIGHT
# endif
#endif


#ifdef  __hpux
#define HPUX
#endif


#if defined(HPUX )|| defined(AIX)
#include <time.h>
#ifdef AIX
#include <sys/time.h>
#endif
#else
#include <sys/time.h>
#endif

#if !defined(DEBUGMODE)
#  define MyFree(x)	if ((x) != NULL) free(x)
#else
#  define free(x)	MyFree(x)
#endif

#ifdef NEXT
#define VOIDSIG int	/* whether signal() returns int of void */
#else
#define VOIDSIG void	/* whether signal() returns int of void */
#endif

#ifdef SOL20
#define OPT_TYPE char	/* opt type for get/setsockopt */
#else
#define OPT_TYPE void
#endif

#ifdef  __osf__
#  define OSF
/* OSF defines BSD to be its version of BSD */
#  undef BSD
#  include <sys/param.h>
#  ifndef BSD
#    define BSD
#  endif
#endif


#ifdef _SEQUENT_                /* Dynix 1.4 or 2.0 Generic Define.. */
#undef BSD
#define SYSV                    /* Also #define SYSV */
#endif

#ifdef  ultrix
#define ULTRIX
#endif

#ifdef  sgi
#define SGI
#endif

#if defined(mips) || defined(PCS)
#undef SYSV
#endif

#ifdef MIPS
#undef BSD
#define BSD             1       /* mips only works in bsd43 environment */
#endif

#ifdef sequent                   /* Dynix (sequent OS) */
#define SEQ_NOFILE    128        /* set to your current kernel impl, */
#endif                           /* max number of socket connections */

#ifdef _SEQUENT_
#define DYNIXPTX
#endif

/*
 * Different name on NetBSD, FreeBSD, BSDI, and Linux
 */
#if defined(__NetBSD__) || defined(__FreeBSD__) || defined(__bsdi__) ||  defined(__linux__)
#define dn_skipname  __dn_skipname
#endif

extern	VOIDSIG	dummy();

#ifdef	DYNIXPTX
#define	NO_U_TYPES
typedef unsigned short n_short;         /* short as received from the net */
typedef unsigned long   n_long;         /* long as received from the net */
typedef unsigned long   n_time;         /* ms since 00:00 GMT, byte rev */
#define _NETINET_IN_SYSTM_INCLUDED
#endif

#ifdef	NO_U_TYPES
typedef	unsigned char	u_char;
typedef	unsigned short	u_short;
typedef	unsigned long	u_long;
typedef	unsigned int	u_int;
#endif

#ifdef	USE_VARARGS
#include <varargs.h>
#endif

#endif /* __sys_include__ */
